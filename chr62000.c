/***************************************************************************** 
 *  Copyright 1998 National Instruments Corporation.  All Rights Reserved.   * 
 *****************************************************************************/

/*****************************************************************************
 *  Chroma DC Power Supply 62000P Series Instrument Driver                               
 *  LabWindows/CVI 6.0 Instrument Driver                                     
 *  Environment: NI IVI Compliance Package 2.1
 *		         CVI Run-Time engine 7.0.0.395
 *               NI-VISA 3.01					  
 *  Original Release: 2005/08/17                                  
 *  By: Bill Lu, Chroma                              
 *      PH. 886-3-3279999 ext:3535   Fax 886-3-3278700                              
 *                                                                           
 *  Modification History:                                                    
 *                                                                           
 *       2005/08/17 - Instrument Driver Created.                  
 *
 *		 2007/06/06	- [1] Added Series/Parallel capability
 *					  [2] Added functions:
 *						  chr62000_SetCurrentSlewINF
 *						  chr62000_SetDcOnVolt
 *						  chr62000_SetBacklight
 *						  chr62000_SetMeasSpeed
 *						  chr62000_SetAVGTimes
 *						  chr62000_SetAVGMethod
 *						  chr62000_SetRmtInhibit
 *						  chr62000_ProgAdd
 *						  chr62000_FetchStatus
 *						  chr62000_QueryMstSlvID
 *						  chr62000_QueryMstPara
 *						  chr62000_QueryMstEnbState
 *						  chr62000_QueryBacklight
 *						  chr62000_QueryRmtInhibit
 *					  [3] Changed CHR62000_MINOR_VERSION to 100
 *
 *		 2008/10/20	- [1] Added USB Interface Support
 *					  [2] Changed CHR62000_MINOR_VERSION to 110 
 *
 *		 2010/05/03	- [1] Added Ethernet Interface Support
 *					  [2] Added functions:
 *						  chr62000_SetOPC
 *						  chr62000_QueryOPC 
 *						  chr62000_Abort
 *						  chr62000_ProgMode
 *						  chr62000_QueryProgMode
 *						  chr62000_SetStepPara
 *					  [3] Added supported models:
 *					      62006P-30-80,62006P-300-8,62012P-40-120,62024P-40-120,62024P-80-60,62024P-100-50,62024P-600-8,62050P-100-100 
 *					  [4] Changed CHR62000_MINOR_VERSION to 200
 *****************************************************************************/

#include <string.h>
#include <ansi_c.h> 
#include <stdio.h>  
#include <formatio.h>
#include <utility.h> 
#include "chr62000.h"

/*****************************************************************************
 *--------------------- Hidden Attribute Declarations -----------------------*
 *****************************************************************************/

#define CHR62000_ATTR_OPC_TIMEOUT      	    (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 1L)   /* ViInt32 */
#define CHR62000_ATTR_BEEPER                (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 2L)
#define CHR62000_ATTR_OUTPUT                (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 3L)
#define CHR62000_ATTR_PROG_SEQ_VOLTAGE      (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 6L)
#define CHR62000_ATTR_PROG_SEQ_VOLT_SLEW    (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 7L)
#define CHR62000_ATTR_PROG_SEQ_CURRENT      (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 8L)
#define CHR62000_ATTR_PROG_SEQ_CURR_SLEW    (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 9L)
#define CHR62000_ATTR_PROG_SEQ_TTL          (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 10L)
#define CHR62000_ATTR_PROG_SEQ_TIME         (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 11L)
#define CHR62000_ATTR_PROG_SEQ_TYPE         (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 12L)
#define CHR62000_ATTR_PROG_LINK             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 13L)
#define CHR62000_ATTR_PROG_COUNT            (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 14L)
#define CHR62000_ATTR_PROG_RUN              (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 15L)
#define CHR62000_ATTR_FOLDBACK_PROTECT      (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 16L)
#define CHR62000_ATTR_APG_MODE              (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 17L)
#define CHR62000_ATTR_APG_REFERENCE_VOLTAGE (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 18L)
#define CHR62000_ATTR_MAXPOWER              (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 19L)
#define CHR62000_ATTR_SIMU_RESISTOR         (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 20L)
#define CHR62000_ATTR_MSTSLV_ID             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 21L)
#define CHR62000_ATTR_PARALLEL_SERIES       (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 22L)
#define CHR62000_ATTR_MASTER_ENABLE         (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 23L)
#define CHR62000_ATTR_CURR_SLEW_INF         (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 24L)
#define CHR62000_ATTR_VOLT_MIN              (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 25L)
#define CHR62000_ATTR_VOLT_MAX              (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 26L)
#define CHR62000_ATTR_CURR_MAX              (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 27L)
#define CHR62000_ATTR_CURR_MIN              (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 28L)
#define CHR62000_ATTR_PWR_MAX               (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 29L)
#define CHR62000_ATTR_PWR_MIN               (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 30L)
#define CHR62000_ATTR_VSLEW_MAX             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 31L)
#define CHR62000_ATTR_VSLEW_MIN             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 32L)
#define CHR62000_ATTR_ISLEW_MAX             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 33L)
#define CHR62000_ATTR_ISLEW_MIN             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 34L)
#define CHR62000_ATTR_VDC_R_MAX             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 35L)
#define CHR62000_ATTR_VDC_F_MIN             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 38L)
#define CHR62000_ATTR_BACKLIGHT             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 39L)
#define CHR62000_ATTR_MEASSPD               (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 40L)
#define CHR62000_ATTR_AVG_TIMES             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 41L)
#define CHR62000_ATTR_INHIBIT               (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 42L)
#define CHR62000_ATTR_PROG_MODE             (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 43L)
#define CHR62000_ATTR_PROG_STEP_TIME        (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 44L)

/*****************************************************************************
 *---------------------------- Useful Macros --------------------------------*
 *****************************************************************************/

    /*- I/O buffer size -----------------------------------------------------*/
#define BUFFER_SIZE                             512L        
#define CHR62000_IO_SESSION_TYPE   IVI_VAL_VISA_SESSION_TYPE

    /*- 488.2 Event Status Register (ESR) Bits ------------------------------*/
#define IEEE_488_2_QUERY_ERROR_BIT              0x04
#define IEEE_488_2_DEVICE_DEPENDENT_ERROR_BIT   0x08
#define IEEE_488_2_EXECUTION_ERROR_BIT          0x10
#define IEEE_488_2_COMMAND_ERROR_BIT            0x20
#define IEEE_488_2_ERROR_BITS                   0x3c


    /*- List of channels passed to the Ivi_BuildChannelTable function -------*/ 
#define CHANNEL_LIST                            "1"

	/*- Internal define value -----------------------------------------------*/ 
#define	accuracy								0.0001	
#define	delay_time								0.00
#define	MaxChannel								100 
#define OVP_RATIO								1.100
#define OCP_RATIO								1.050
#define OPP_RATIO								1.050
#define ON_STRING								"ON"
#define OFF_STRING								"OFF" 
#define master_enable_delay_time				2.00 
#define Do_not_write							-99999.11
#define flag_plus								1
#define flag_minus								0
#define Invalid_Max								1.0
#define Invalid_Min								0.9
#define Slew_INF								"INF."
#define ASCII_INF								"1e9999999999"
#define prog_curr_slew_inf						0
#define normal_timeout_5000						5000
#define short_timeout_200						200

static IviRangeTableEntry attrProgAddRangeTableEntries[] =
	{
		{1, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgAddRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgAddRangeTableEntries,
	};

static IviRangeTableEntry attrProgSeqRestRangeTableEntries[] =
	{
		{1, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSeqRestRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSeqRestRangeTableEntries,
	};

static IviRangeTableEntry attrProgStepEndvRangeTableEntries[] =
	{
		{0.00, 80.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgStepEndvRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgStepEndvRangeTableEntries,
	};

static IviRangeTableEntry attrProgStepStartvRangeTableEntries[] =
	{
		{0.00, 80.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgStepStartvRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgStepStartvRangeTableEntries,
	};

static IviRangeTableEntry attrProgStepTimeRangeTableEntries[] =
	{
		{0.00, 359999.99, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgStepTimeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgStepTimeRangeTableEntries,
	};

static IviRangeTableEntry attrProgModeRangeTableEntries[] =
	{
		{CHR62000_VAL_PROG_LIST, 0, 0, "LIST", 0},
		{CHR62000_VAL_PROG_STEP, 0, 0, "STEP", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgModeRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgModeRangeTableEntries,
	};

static IviRangeTableEntry attrInhibitRangeTableEntries[] =
	{
		{CHR62000_VAL_INHIBIT_OFF, 0, 0, "OFF", 0},
		{CHR62000_VAL_INHIBIT_TRIG, 0, 0, "TRIG", 0},
		{CHR62000_VAL_INHIBIT_LIVE, 0, 0, "LIVE", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrInhibitRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrInhibitRangeTableEntries,
	};

static IviRangeTableEntry attrAvgTimesRangeTableEntries[] =
	{
		{CHR62000_VAL_AVG_TIMES_1, 0, 0, "0", 0},
		{CHR62000_VAL_AVG_TIMES_2, 0, 0, "1", 0},
		{CHR62000_VAL_AVG_TIMES_4, 0, 0, "2", 0},
		{CHR62000_VAL_AVG_TIMES_8, 0, 0, "3", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrAvgTimesRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrAvgTimesRangeTableEntries,
	};

static IviRangeTableEntry attrMeasspdRangeTableEntries[] =
	{
		{CHR62000_VAL_MEASSPD_240, 0, 0, "0", 0},
		{CHR62000_VAL_MEASSPD_120, 0, 0, "1", 0},
		{CHR62000_VAL_MEASSPD_60, 0, 0, "2", 0},
		{CHR62000_VAL_MEASSPD_30, 0, 0, "3", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrMeasspdRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrMeasspdRangeTableEntries,
	};

static IviRangeTableEntry attrBacklightRangeTableEntries[] =
	{
		{CHR62000_VAL_BACKL_OFF, 0, 0, "OFF", 0},
		{CHR62000_VAL_BACKL_DIM, 0, 0, "DIM", 0},
		{CHR62000_VAL_BACKL_NOR, 0, 0, "NOR", 0},
		{CHR62000_VAL_BACKL_HIGH, 0, 0, "HIGH", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrBacklightRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrBacklightRangeTableEntries,
	};

static IviRangeTableEntry attrVdcOnRiseRangeTableEntries[] =
	{
		{0.5, 79.5, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVdcOnRiseRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVdcOnRiseRangeTableEntries,
	};

static IviRangeTableEntry attrVdcOnFallRangeTableEntries[] =
	{
		{0.5, 79.5, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVdcOnFallRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVdcOnFallRangeTableEntries,
	};


static IviRangeTableEntry attrSlaveNumbersRangeTableEntries[] =
	{
		{1, 4, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSlaveNumbersRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSlaveNumbersRangeTableEntries,
	};

static IviRangeTableEntry attrMstslvIdRangeTableEntries[] =
	{
		{CHR62000_VAL_MASTER, 0, 0, "MASTER", 0},
		{CHR62000_VAL_SLAVE1, 0, 0, "SLAVE1", 0},
		{CHR62000_VAL_SLAVE2, 0, 0, "SLAVE2", 0},
		{CHR62000_VAL_SLAVE3, 0, 0, "SLAVE3", 0},
		{CHR62000_VAL_SLAVE4, 0, 0, "SLAVE4", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrMstslvIdRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrMstslvIdRangeTableEntries,
	};

static IviRangeTableEntry attrStatusSreRangeTableEntries[] =
	{
		{0, 255, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrStatusSreRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrStatusSreRangeTableEntries,
	};

static IviRangeTableEntry attrStatusEseRangeTableEntries[] =
	{
		{0, 255, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrStatusEseRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrStatusEseRangeTableEntries,
	};

static IviRangeTableEntry ModelRatingEntries[] =
	{
		{CHR62000_VAL_62006P_100_25_V, 0, 0, "", 0},
		{CHR62000_VAL_62006P_100_25_I, 0, 0, "", 0},
		{CHR62000_VAL_62006P_100_25_P, 0, 0, "", 0},
		{CHR62000_VAL_62006P_100_25_VSR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62006P_100_25_VSR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62006P_100_25_ISR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62006P_100_25_ISR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62006P_100_25_DCON_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62006P_100_25_DCON_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_30_160_V, 0, 0, "", 0},
		{CHR62000_VAL_62012P_30_160_I, 0, 0, "", 0},
		{CHR62000_VAL_62012P_30_160_P, 0, 0, "", 0},
		{CHR62000_VAL_62012P_30_160_VSR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_30_160_VSR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_30_160_ISR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_30_160_ISR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_30_160_DCON_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_30_160_DCON_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_80_60_V, 0, 0, "", 0},
		{CHR62000_VAL_62012P_80_60_I, 0, 0, "", 0},
		{CHR62000_VAL_62012P_80_60_P, 0, 0, "", 0},
		{CHR62000_VAL_62012P_80_60_VSR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_80_60_VSR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_80_60_ISR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_80_60_ISR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_80_60_DCON_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_80_60_DCON_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_100_50_V, 0, 0, "", 0},
		{CHR62000_VAL_62012P_100_50_I, 0, 0, "", 0},
		{CHR62000_VAL_62012P_100_50_P, 0, 0, "", 0},
		{CHR62000_VAL_62012P_100_50_VSR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_100_50_VSR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_100_50_ISR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_100_50_ISR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_100_50_DCON_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_100_50_DCON_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_600_8_V, 0, 0, "", 0},
		{CHR62000_VAL_62012P_600_8_I, 0, 0, "", 0},
		{CHR62000_VAL_62012P_600_8_P, 0, 0, "", 0},
		{CHR62000_VAL_62012P_600_8_VSR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_600_8_VSR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_600_8_ISR_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_600_8_ISR_MAX, 0, 0, "", 0},
		{CHR62000_VAL_62012P_600_8_DCON_MIN, 0, 0, "", 0},
		{CHR62000_VAL_62012P_600_8_DCON_MAX, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable ModelRating =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        ModelRatingEntries,
	};

static IviRangeTableEntry attrFoldbackDelayTimeRangeTableEntries[] =
	{
		{0.01, 600.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrFoldbackDelayTimeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrFoldbackDelayTimeRangeTableEntries,
	};

static IviRangeTableEntry attrApgModeRangeTableEntries[] =
	{
		{CHR62000_VAL_APG_NONE, 0, 0, "NONE", 0},
		{CHR62000_VAL_APG_V, 0, 0, "V", 0},
		{CHR62000_VAL_APG_I, 0, 0, "I", 0},
		{CHR62000_VAL_APG_VI, 0, 0, "VI", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrApgModeRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrApgModeRangeTableEntries,
	};

static IviRangeTableEntry attrFoldbackProtectRangeTableEntries[] =
	{
		{CHR62000_VAL_FOLD_DISABLE, 0, 0, "DISABLE", 0},
		{CHR62000_VAL_FOLD_CVTOCC, 0, 0, "CVTOCC", 0},
		{CHR62000_VAL_FOLD_CCTOCV, 0, 0, "CCTOCV", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrFoldbackProtectRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrFoldbackProtectRangeTableEntries,
	};

static IviRangeTableEntry attrProgSeqVoltageRangeTableEntries[] =
	{
		{0.00, 80.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSeqVoltageRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSeqVoltageRangeTableEntries,
	};

static IviRangeTableEntry attrProgSeqVoltSlewRangeTableEntries[] =
	{
		{0.01, 10.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSeqVoltSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSeqVoltSlewRangeTableEntries,
	};

static IviRangeTableEntry attrProgSeqCurrentRangeTableEntries[] =
	{
		{0.00, 60.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSeqCurrentRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSeqCurrentRangeTableEntries,
	};

static IviRangeTableEntry attrProgSeqCurrSlewRangeTableEntries[] =
	{
		{0.01, 1.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSeqCurrSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSeqCurrSlewRangeTableEntries,
	};

static IviRangeTableEntry attrProgSeqTtlRangeTableEntries[] =
	{
		{0, 255, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSeqTtlRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSeqTtlRangeTableEntries,
	};

static IviRangeTableEntry attrProgSeqTimeRangeTableEntries[] =
	{
		{0.00, 15000.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSeqTimeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSeqTimeRangeTableEntries,
	};

static IviRangeTableEntry attrProgSeqSelectedRangeTableEntries[] =
	{
		{1, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSeqSelectedRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSeqSelectedRangeTableEntries,
	};

static IviRangeTableEntry attrProgLinkRangeTableEntries[] =
	{
		{0, 10, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgLinkRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgLinkRangeTableEntries,
	};

static IviRangeTableEntry attrProgCountRangeTableEntries[] =
	{
		{1, 15000, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgCountRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgCountRangeTableEntries,
	};

static IviRangeTableEntry attrProgSelectedRangeTableEntries[] =
	{
		{1, 10, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSelectedRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSelectedRangeTableEntries,
	};

static IviRangeTableEntry attrProgSeqTypeRangeTableEntries[] =
	{
		{CHR62000_VAL_AUTO, 0, 0, "AUTO", 0},
		{CHR62000_VAL_MANUAL, 0, 0, "MANUAL", 0},
		{CHR62000_VAL_TRIGGER, 0, 0, "TRI", 0},
		{CHR62000_VAL_SKIP, 0, 0, "SKIP", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgSeqTypeRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgSeqTypeRangeTableEntries,
	};

static IviRangeTableEntry attrTtlPortRangeTableEntries[] =
	{
		{0, 255, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrTtlPortRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrTtlPortRangeTableEntries,
	};



static IviRangeTableEntry attrVoltLimitHighRangeTableEntries[] =
	{
		{0.0, 80.0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVoltLimitHighRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVoltLimitHighRangeTableEntries,
	};

static IviRangeTableEntry attrVoltLimitLowRangeTableEntries[] =
	{
		{0.0, 80.0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVoltLimitLowRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVoltLimitLowRangeTableEntries,
	};

static IviRangeTableEntry attrVoltProtectRangeTableEntries[] =
	{
		{0.0, 88.0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVoltProtectRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVoltProtectRangeTableEntries,
	};

static IviRangeTableEntry attrVoltSlewRangeTableEntries[] =
	{
		{0.01, 10.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVoltSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVoltSlewRangeTableEntries,
	};

static IviRangeTableEntry attrCurrentRangeTableEntries[] =
	{
		{0.00, 60.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentRangeTableEntries,
	};

static IviRangeTableEntry attrCurrLimitHighRangeTableEntries[] =
	{
		{0.0, 60.0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrLimitHighRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrLimitHighRangeTableEntries,
	};

static IviRangeTableEntry attrCurrLimitLowRangeTableEntries[] =
	{
		{0.0, 60.0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrLimitLowRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrLimitLowRangeTableEntries,
	};

static IviRangeTableEntry attrCurrProtectRangeTableEntries[] =
	{
		{0.0, 63.0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrProtectRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrProtectRangeTableEntries,
	};

static IviRangeTableEntry attrCurrSlewRangeTableEntries[] =
	{
		{0.01, 1.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrSlewRangeTableEntries,
	};

static IviRangeTableEntry attrPowerProtectRangeTableEntries[] =
	{
		{0, 1260.0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrPowerProtectRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrPowerProtectRangeTableEntries,
	};

static IviRangeTableEntry attrFetchTypeRangeTableEntries[] =
	{
		{CHR62000_VAL_FETCH_VOLT, 0, 0, "VOLT?", 0},
		{CHR62000_VAL_FETCH_CURR, 0, 0, "CURR?", 0},
		{CHR62000_VAL_FETCH_POWER, 0, 0, "POW?", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrFetchTypeRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrFetchTypeRangeTableEntries,
	};

static IviRangeTableEntry attrMeasTypeRangeTableEntries[] =
	{
		{CHR62000_VAL_MEAS_VOLT, 0, 0, "VOLT?", 0},
		{CHR62000_VAL_MEAS_CURR, 0, 0, "CURR?", 0},
		{CHR62000_VAL_MEAS_POWER, 0, 0, "POW?", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrMeasTypeRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrMeasTypeRangeTableEntries,
	};

static IviRangeTableEntry attrVoltageRangeTableEntries[] =
	{
		{0.00, 80.00, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVoltageRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVoltageRangeTableEntries,
	};

/*****************************************************************************
 *-------------- Utility Function Declarations (Non-Exported) ---------------*
 *****************************************************************************/
static ViStatus chr62000_InitAttributes (ViSession vi);
static ViStatus chr62000_DefaultInstrSetup (ViSession openInstrSession);
static ViStatus chr62000_CheckStatus (ViSession vi);
static ViStatus chr62000_WaitForOPC (ViSession vi, ViInt32 maxTime);

ViStatus _VI_FUNC chr62000_Invalidate_Program (ViSession vi, ViInt32 programSelected);
ViStatus _VI_FUNC chr62000_Simulate_Meas_Fetch (ViSession vi, ViInt32 measureType, ViReal64 *readValue);
ViStatus _VI_FUNC chr62000_SelectModelMaxRating (ViSession vi);
ViStatus _VI_FUNC chr62000_MstSlv_Status (ViSession vi);
ViStatus _VI_FUNC chr62000_AttrFlags_Handle (ViSession vi, ViAttr attributeId, ViInt32 plus_minus, IviAttrFlags flags);
ViStatus _VI_FUNC chr62000_Specify_Cache (ViSession vi, ViInt32 group, ViBoolean cache);
ViStatus _VI_FUNC chr62000_SwapAttributeViReal64MinMax (ViSession vi, ViAttr attributeId, ViReal64 min, ViReal64 max);
ViStatus _VI_FUNC chr62000_SwapAttributeViReal64MinMax1 (ViSession vi, ViAttr attributeId, ViReal64 min, ViReal64 max, IviRangeTablePtr *rangeTablePtr);
ViStatus _VI_FUNC chr62000_Digit_boundary (ViReal64 *value, ViInt32 digit);
ViStatus _VI_FUNC chr62000_Detect_error (ViSession vi, ViChar Cmd[]); 
ViStatus _VI_FUNC chr62000_MST_APG_PRG_status (ViSession vi, ViInt32 condition);
ViStatus _VI_FUNC chr62000_QueryMaxMin (ViSession vi);

    /*- File I/O Utility Functions -*/
static ViStatus chr62000_ReadToFile (ViSession vi, ViConstString filename, 
                                     ViInt32 maxBytesToRead, ViInt32 fileAction, ViInt32 *totalBytesWritten);
static ViStatus chr62000_WriteFromFile (ViSession vi, ViConstString filename, 
                                        ViInt32 maxBytesToWrite, ViInt32 byteOffset, 
                                        ViInt32 *totalBytesWritten);

/*****************************************************************************
 *----------------- Callback Declarations (Non-Exported) --------------------*
 *****************************************************************************/

    /*- Global Session Callbacks --------------------------------------------*/
    
static ViStatus _VI_FUNC chr62000_CheckStatusCallback (ViSession vi, ViSession io);
static ViStatus _VI_FUNC chr62000_WaitForOPCCallback (ViSession vi, ViSession io);

    /*- Attribute callbacks -------------------------------------------------*/

static ViStatus _VI_FUNC chr62000AttrDriverRevision_ReadCallback (ViSession vi,
                                                                  ViSession io, 
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId, 
                                                                  const ViConstString cacheValue);
static ViStatus _VI_FUNC chr62000AttrInstrumentManufacturer_ReadCallback (ViSession vi, 
                                                                          ViSession io,
                                                                          ViConstString channelName, 
                                                                          ViAttr attributeId,
                                                                          const ViConstString cacheValue);
static ViStatus _VI_FUNC chr62000AttrInstrumentModel_ReadCallback (ViSession vi, 
                                                                   ViSession io,
                                                                   ViConstString channelName, 
                                                                   ViAttr attributeId,
                                                                   const ViConstString cacheValue);

static ViStatus _VI_FUNC chr62000AttrFirmwareRevision_ReadCallback (ViSession vi, 
                                                                    ViSession io,
                                                                    ViConstString channelName, 
                                                                    ViAttr attributeId,
                                                                    const ViConstString cacheValue);
static ViStatus _VI_FUNC chr62000AttrIdQueryResponse_ReadCallback (ViSession vi,
                                                                   ViSession io, 
                                                                   ViConstString channelName, 
                                                                   ViAttr attributeId, 
                                                                   const ViConstString cacheValue);

static ViStatus _VI_FUNC chr62000AttrVoltage_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVoltage_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVoltLimitHigh_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVoltLimitHigh_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVoltLimitLow_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVoltLimitLow_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVoltProtect_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVoltProtect_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVoltSlew_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVoltSlew_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrCurrent_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrCurrent_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrCurrLimitHigh_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrCurrLimitHigh_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrCurrLimitLow_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrCurrLimitLow_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrCurrProtect_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrCurrProtect_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrCurrSlew_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrCurrSlew_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrPowerProtect_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrPowerProtect_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value);


static ViStatus _VI_FUNC chr62000AttrOutput_WriteCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViBoolean value);

static ViStatus _VI_FUNC chr62000AttrTtlPort_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrTtlPort_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrBeeper_ReadCallback (ViSession vi,
                                                          ViSession io,
                                                          ViConstString channelName,
                                                          ViAttr attributeId,
                                                          ViBoolean *value);

static ViStatus _VI_FUNC chr62000AttrBeeper_WriteCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViBoolean value);


static ViStatus _VI_FUNC chr62000AttrRemoteMode_WriteCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViBoolean value);

static ViStatus _VI_FUNC chr62000AttrFoldbackDelayTime_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrFoldbackDelayTime_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrFoldbackProtect_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrFoldbackProtect_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrApgMode_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrApgMode_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrApgReferenceVoltage_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViBoolean *value);

static ViStatus _VI_FUNC chr62000AttrApgReferenceVoltage_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean value);

static ViStatus _VI_FUNC chr62000AttrProgSelected_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrProgSelected_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltage_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltage_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltSlew_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltSlew_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrent_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrent_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrSlew_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrSlew_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrProgSeqTtl_ReadCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrProgSeqTtl_WriteCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrProgSeqTime_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrProgSeqTime_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrProgSeqType_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrProgSeqType_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrProgLink_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrProgLink_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrProgCount_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrProgCount_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrProgSeqSelected_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrProgSeqSelected_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrProgRun_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViBoolean *value);

static ViStatus _VI_FUNC chr62000AttrProgRun_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean value);

static ViStatus _VI_FUNC chr62000AttrStatusEse_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrStatusEse_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrStatusSre_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrStatusSre_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrMstslvId_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrMstslvId_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrMasterEnable_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean *value);

static ViStatus _VI_FUNC chr62000AttrMasterEnable_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean value);

static ViStatus _VI_FUNC chr62000AttrParallelSeries_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean *value);

static ViStatus _VI_FUNC chr62000AttrParallelSeries_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViBoolean value);

static ViStatus _VI_FUNC chr62000AttrSlaveNumbers_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrSlaveNumbers_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value);


static ViStatus _VI_FUNC chr62000AttrCurrSlewInf_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean value);

static ViStatus _VI_FUNC chr62000AttrVdcOnFall_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVdcOnFall_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVdcOnRise_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVdcOnRise_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVoltMin_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVoltMin_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVoltMax_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVoltMax_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVoltage_RangeTableCallback (ViSession vi,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrCurrMax_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrCurrMax_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrCurrMin_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrCurrMin_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrIslewMax_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrIslewMax_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrIslewMin_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrIslewMin_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrPwrMax_ReadCallback (ViSession vi,
                                                          ViSession io,
                                                          ViConstString channelName,
                                                          ViAttr attributeId,
                                                          ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrPwrMax_WriteCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrPwrMin_ReadCallback (ViSession vi,
                                                          ViSession io,
                                                          ViConstString channelName,
                                                          ViAttr attributeId,
                                                          ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrPwrMin_WriteCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 value);



static ViStatus _VI_FUNC chr62000AttrVdcFMin_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVdcFMin_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVdcRMax_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVdcRMax_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value);



static ViStatus _VI_FUNC chr62000AttrVslewMax_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVslewMax_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVslewMin_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrVslewMin_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrVoltLimitHigh_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrVoltLimitLow_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrVoltProtect_RangeTableCallback (ViSession vi,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrVdcOnFall_RangeTableCallback (ViSession vi,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrVdcOnRise_RangeTableCallback (ViSession vi,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrVoltSlew_RangeTableCallback (ViSession vi,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrCurrLimitHigh_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrCurrLimitLow_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrCurrProtect_RangeTableCallback (ViSession vi,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrCurrSlew_RangeTableCallback (ViSession vi,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrCurrent_RangeTableCallback (ViSession vi,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrPowerProtect_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrBacklight_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrBacklight_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrMeasspd_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrMeasspd_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrAvgMethod_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViBoolean *value);

static ViStatus _VI_FUNC chr62000AttrAvgMethod_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViBoolean value);

static ViStatus _VI_FUNC chr62000AttrAvgTimes_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrInhibit_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrInhibit_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrent_RangeTableCallback (ViSession vi,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrSlew_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltSlew_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltage_RangeTableCallback (ViSession vi,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrProgMode_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 *value);

static ViStatus _VI_FUNC chr62000AttrProgMode_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrProgStepTime_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrProgStepTime_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrProgStepStartv_RangeTableCallback (ViSession vi,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrProgStepStartv_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrProgStepStartv_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrProgStepEndv_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrProgStepEndv_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value);

static ViStatus _VI_FUNC chr62000AttrProgStepEndv_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value);

static ViStatus _VI_FUNC chr62000AttrProgAdd_RangeTableCallback (ViSession vi,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC chr62000AttrProgAdd_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value);

static ViStatus _VI_FUNC chr62000AttrProgSeqRest_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 *value);


ViInt32 g_nBaudRate;
                                                                        
                                                                        
/*****************************************************************************
 *------------ Instrument-specific public Functions (Non-Exported Functions) ---------------*
 *****************************************************************************/
 
/*****************************************************************************
 * Function: chr62000_Simulate_Meas_Fetch   
 * Purpose:  This function simulate the measure and fetch functions when 
 *           Ivi_Simulating
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Simulate_Meas_Fetch (ViSession vi, ViInt32 measureType, ViReal64 *readValue)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	simulated_value=0,Resistor=0;
	ViBoolean	output_state=VI_FALSE;

	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_OUTPUT, 0,
											&output_state));	
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_SIMU_RESISTOR, 0,
											&Resistor));
												
	if ( output_state == VI_TRUE)
	{
		switch (measureType)
		{
			case 0:	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLTAGE, IVI_VAL_DIRECT_USER_CALL,
											&simulated_value));
					(*readValue)=simulated_value+((ViReal64)(rand()%30)/100000); 
					break;
			case 1:	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLTAGE, IVI_VAL_DIRECT_USER_CALL,
											&simulated_value));
					(*readValue)=(simulated_value+((ViReal64)(rand()%40)/10000))/Resistor;
					break;
			case 2:	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLTAGE, IVI_VAL_DIRECT_USER_CALL,
											&simulated_value));
					(*readValue)=(simulated_value*simulated_value+((ViReal64)(rand()%50)/10000))/Resistor;
					break;
			default:
					break;
		}
	}
	else
		(*readValue)=0.00;
	
Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_MstSlv_Status   
 * Purpose:  This function check the Master/Slave status when initialize, 
 *           and increase rating or set attribute never cache to slaves 
 *			 if master control is enabled
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_MstSlv_Status (ViSession vi)
{
	ViStatus    error = VI_SUCCESS;
	ViBoolean	pre_masterEnable = VI_TRUE; 
	ViInt32		MSTSLV_ID=-1;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));  

	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_MASTER_ENABLE, 0,
								&pre_masterEnable));
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_MSTSLV_ID, 0,
								&MSTSLV_ID));
								
	if ( MSTSLV_ID!=0 && pre_masterEnable!=VI_TRUE) // if MSTSLV is slave mode and not enabled, force to Master mode to prevent some complex situation
		checkErr( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_MSTSLV_ID, 0, 0));
						
	if ( pre_masterEnable==VI_TRUE)												
	{
		if ( MSTSLV_ID==0 ) // if MSTSLV=Master, then increase rating when initialize
			chr62000_Specify_Cache (vi, 2, VI_TRUE);
		else // in MSTSLV=Slave, then set attribute never cache when initialize
			chr62000_Specify_Cache (vi, 1, VI_FALSE);
	}	
	else
		chr62000_Specify_Cache (vi, 2, VI_FALSE); // if MST Enable=False, restore single machine rating
	checkErr( chr62000_CheckStatus (vi));

Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_AttrFlags_Handle   
 * Purpose:  This function add or remove the flags from attribute ID
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_AttrFlags_Handle (ViSession vi, ViAttr attributeId, ViInt32 plus_minus, IviAttrFlags flags)  
{
	ViStatus    error = VI_SUCCESS;
	IviAttrFlags	org_flags=-1; 
	
	checkErr( Ivi_GetAttributeFlags (vi, attributeId, &org_flags));   		
	
	if ( plus_minus==flag_plus )
		checkErr( Ivi_SetAttributeFlags (vi, attributeId, (org_flags | flags)));
	else if ( plus_minus==flag_minus )
		checkErr( Ivi_SetAttributeFlags (vi, attributeId, (org_flags & (~flags))));	
	
Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_Specify_Cache   
 * Purpose:  This function sets cache or never cache of attributes, and 
 *			 increase rating or restore rating when Master/Slave status changed
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Specify_Cache (ViSession vi, ViInt32 group, ViBoolean cache)
{
	ViStatus    error = VI_SUCCESS; 
	ViBoolean	parallelSeries = VI_TRUE;
	ViInt32		slaveNumbers=0;
	ViReal64	vireal64_max=0,vireal64_min=0,nums=0,volt_max=0,curr_max=0,power_max=0; 
	ViChar		rdBuffer[BUFFER_SIZE]; 
	IviAttrFlags	flags=-1;
	ViInt32		ID_NUMS=20,ID_INDEX=0,flag_operation=-1;
	ViAttr		ID[20]={CHR62000_ATTR_VOLTAGE, CHR62000_ATTR_VOLT_LIMIT_HIGH, CHR62000_ATTR_VOLT_LIMIT_LOW, CHR62000_ATTR_VOLT_PROTECT,
						CHR62000_ATTR_VOLT_SLEW, CHR62000_ATTR_CURRENT, CHR62000_ATTR_CURR_LIMIT_HIGH, CHR62000_ATTR_CURR_LIMIT_LOW,
						CHR62000_ATTR_CURR_PROTECT, CHR62000_ATTR_CURR_SLEW, CHR62000_ATTR_CURR_SLEW_INF, CHR62000_ATTR_POWER_PROTECT, 						
						CHR62000_ATTR_FOLDBACK_PROTECT, CHR62000_ATTR_FOLDBACK_DELAY_TIME, CHR62000_ATTR_APG_MODE, CHR62000_ATTR_APG_REFERENCE_VOLTAGE, 						
						CHR62000_ATTR_MSTSLV_ID, CHR62000_ATTR_PARALLEL_SERIES, CHR62000_ATTR_SLAVE_NUMBERS, CHR62000_ATTR_MASTER_ENABLE
						};
						
	switch (group) 
	{
		case 1: // set attribute cache or never cache
				if ( cache==VI_TRUE ) 
					flag_operation=flag_minus; //set attribute cache 
				else 
					flag_operation=flag_plus;  //set attribute never cache 
					
				for (ID_INDEX=0;ID_INDEX<ID_NUMS;ID_INDEX++)				
					checkErr( chr62000_AttrFlags_Handle (vi, ID[ID_INDEX], flag_operation, IVI_VAL_NEVER_CACHE)); 
					
				checkErr( Ivi_InvalidateAllAttributes (vi));
				break;
		case 2: // increase rating or restore single machine rating
				
				checkErr( Ivi_InvalidateAllAttributes (vi));
				checkErr( chr62000_QueryMaxMin (vi)); 
				break;
		default:
				break;
	}
	
Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_SelectModelMaxRating   
 * Purpose:  Exchange the max. rating of attributes by different 
 *           DC Power Supply model
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SelectModelMaxRating (ViSession vi)
{
	ViStatus    error = VI_SUCCESS; 
	ViChar		rdBuffer[BUFFER_SIZE]; 
	ViInt32		ModelIndex=18;
	ViReal64	volt_max=0,curr_max=0,power_max=0,vsr_min=0,vsr_max=0,isr_min=0,isr_max=0,dcon_min=0,dcon_max=0;
	
	checkErr( Ivi_GetAttributeViString (vi, VI_NULL, CHR62000_ATTR_INSTRUMENT_MODEL,
							  			IVI_VAL_DIRECT_USER_CALL, BUFFER_SIZE, rdBuffer));
	
	ModelIndex=18;
	if ( strncmp(rdBuffer,"62006P-100-25",13) == 0 )							  			
		ModelIndex=0;
	else if ( strncmp(rdBuffer,"62012P-30-160",13) == 0 )
		ModelIndex=9;	
	else if ( strncmp(rdBuffer,"62012P-80-60",12) == 0 )
		ModelIndex=18;	
	else if ( strncmp(rdBuffer,"62012P-100-50",13) == 0 )
		ModelIndex=27;	
	else if ( strncmp(rdBuffer,"62012P-600-8",12) == 0 )
		ModelIndex=36;
		
	checkErr( Ivi_GetViReal64EntryFromIndex (ModelIndex, &ModelRating, &volt_max, VI_NULL, VI_NULL, VI_NULL, VI_NULL));
	checkErr( Ivi_GetViReal64EntryFromIndex (ModelIndex+1, &ModelRating, &curr_max, VI_NULL, VI_NULL, VI_NULL, VI_NULL)); 
	checkErr( Ivi_GetViReal64EntryFromIndex (ModelIndex+2, &ModelRating, &power_max, VI_NULL, VI_NULL, VI_NULL, VI_NULL));
	checkErr( Ivi_GetViReal64EntryFromIndex (ModelIndex+3, &ModelRating, &vsr_min, VI_NULL, VI_NULL, VI_NULL, VI_NULL)); 
	checkErr( Ivi_GetViReal64EntryFromIndex (ModelIndex+4, &ModelRating, &vsr_max, VI_NULL, VI_NULL, VI_NULL, VI_NULL)); 
	checkErr( Ivi_GetViReal64EntryFromIndex (ModelIndex+5, &ModelRating, &isr_min, VI_NULL, VI_NULL, VI_NULL, VI_NULL)); 
	checkErr( Ivi_GetViReal64EntryFromIndex (ModelIndex+6, &ModelRating, &isr_max, VI_NULL, VI_NULL, VI_NULL, VI_NULL)); 
	checkErr( Ivi_GetViReal64EntryFromIndex (ModelIndex+7, &ModelRating, &dcon_min, VI_NULL, VI_NULL, VI_NULL, VI_NULL)); 
	checkErr( Ivi_GetViReal64EntryFromIndex (ModelIndex+8, &ModelRating, &dcon_max, VI_NULL, VI_NULL, VI_NULL, VI_NULL)); 
	
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_VOLTAGE, Do_not_write, volt_max) );
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_VOLT_LIMIT_HIGH, Do_not_write, volt_max) ); 
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_VOLT_LIMIT_LOW, Do_not_write, volt_max) ); 
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_VOLT_PROTECT, Do_not_write, volt_max*OVP_RATIO) ); 
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_PROG_SEQ_VOLTAGE, Do_not_write, volt_max) ); 
	
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_CURRENT, Do_not_write, curr_max) );
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_CURR_LIMIT_HIGH, Do_not_write, curr_max) ); 
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_CURR_LIMIT_LOW, Do_not_write, curr_max) ); 
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_CURR_PROTECT, Do_not_write, curr_max*OCP_RATIO) ); 
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_PROG_SEQ_CURRENT, Do_not_write, curr_max) ); 
	
	checkErr( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_MAXPOWER, 0, power_max));
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_POWER_PROTECT, Do_not_write, power_max*OPP_RATIO) );
	
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_VOLT_SLEW, vsr_min, vsr_max) );
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_PROG_SEQ_VOLT_SLEW, vsr_min, vsr_max) ); 
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_CURR_SLEW, isr_min, isr_max) );
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_PROG_SEQ_CURR_SLEW, isr_min, isr_max) );
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_VDC_ON_RISE, dcon_min, dcon_max) );
	checkErr( chr62000_SwapAttributeViReal64MinMax (vi, CHR62000_ATTR_VDC_ON_FALL, dcon_min, dcon_max) );  
	
	// decide the CHR62000_ATTR_SIMU_RESISTOR,  
	// power_max*0.7 < (volt_max*volt_max*CHR62000_ATTR_SIMU_RESISTOR) < power_max*0.9
	if (1)
	{
		ViReal64	a=0,res_max,res_min,act_ers,mult=1;
		srand( (unsigned) time(NULL) );
		res_max=(volt_max*volt_max)/(power_max*0.7);
		res_min=(volt_max*volt_max)/(power_max*0.9);  
		
		if ( res_max < 100)
			mult=100,res_max*=mult,res_min*=mult;
			
		while ( (a=(rand()%((ViInt32)res_max))) < res_min);

		checkErr( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_SIMU_RESISTOR, 0, a/mult)); 
	}
	
Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_SwapAttributeViReal64MinMax
 * Purpose:  A subroutine called by chr62000_SelectModelMaxRating
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SwapAttributeViReal64MinMax (ViSession vi, ViAttr attributeId, ViReal64 min, ViReal64 max)
{
	ViStatus    error = VI_SUCCESS;   
	IviRangeTablePtr	tblPtr=VI_NULL;
	ViReal64	org_min=-1,org_max=-1;

	checkErr( Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));
	checkErr( Ivi_GetViReal64EntryFromIndex (0, tblPtr, &org_min, &org_max, VI_NULL, VI_NULL, VI_NULL));
	if ( min==Do_not_write )
		checkErr( Ivi_SetRangeTableEntry (tblPtr, 0, org_min, max, VI_NULL, VI_NULL, VI_NULL));
	else if ( max==Do_not_write )		
		checkErr( Ivi_SetRangeTableEntry (tblPtr, 0, min, org_max, VI_NULL, VI_NULL, VI_NULL));  		
	else	
		checkErr( Ivi_SetRangeTableEntry (tblPtr, 0, min, max, VI_NULL, VI_NULL, VI_NULL));  
	//checkErr( Ivi_SetStoredRangeTablePtr (vi, attributeId, tblPtr));	

Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryMaxMin   
 * Purpose:  Query DC Power Supply's V/I/P .. Max & Min
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryMaxMin (ViSession vi)
{
	ViStatus    error = VI_SUCCESS; 
	ViReal64	value=0,nums=0,vireal64_max=0,vireal64_min=0;
	ViBoolean	parallelSeries = VI_TRUE, status = VI_TRUE, pre_masterEnable = VI_TRUE;
	ViInt32		slaveNumbers=0,MSTSLV_ID=-1;
	ViChar		rdBuffer[BUFFER_SIZE]; 

	if (!Ivi_Simulating(vi))
	{
	ViSession   io = Ivi_IOSession(vi);
	checkErr( Ivi_GetAttributeViString (vi, VI_NULL, CHR62000_ATTR_INSTRUMENT_MODEL,
							  			IVI_VAL_DIRECT_USER_CALL, BUFFER_SIZE, rdBuffer));
		if ( strncmp(rdBuffer,"620",3) == 0 )							  			
		{
		checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_MASTER_ENABLE, 0,
											&pre_masterEnable));
		checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_MSTSLV_ID, 0,
											&MSTSLV_ID));
		if ( !(MSTSLV_ID!=0 && pre_masterEnable==VI_TRUE) )
			{
			viCheckErr( viSetAttribute (io, VI_ATTR_TMO_VALUE, short_timeout_200 ));
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, Invalid_Min);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, Invalid_Max);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MIN, 0, Invalid_Min);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_CURR_MIN);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MIN, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MAX, 0, Invalid_Max);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_CURR_MAX);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MAX, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PWR_MIN, 0, Invalid_Min);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_PWR_MIN);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PWR_MIN, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PWR_MAX, 0, Invalid_Max);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_PWR_MAX);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PWR_MAX, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MIN, 0, Invalid_Min);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VSLEW_MIN);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MIN, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MAX, 0, Invalid_Max);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VSLEW_MAX);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MAX, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_ISLEW_MIN, 0, Invalid_Min);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_ISLEW_MIN);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_ISLEW_MIN, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_ISLEW_MAX, 0, Invalid_Max);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_ISLEW_MAX);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_ISLEW_MAX, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_F_MIN, 0, Invalid_Min);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VDC_F_MIN);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_F_MIN, 0, &value);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_R_MAX, 0, Invalid_Max);
			Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VDC_R_MAX);
			Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_R_MAX, 0, &value);
			checkErr( chr62000_SetCLS(vi));
			viCheckErr( viSetAttribute (io, VI_ATTR_TMO_VALUE, normal_timeout_5000 ));
			}
		}		
	}
	else // increase rating or restore single machine rating 
	{
	chr62000_SelectModelMaxRating (vi); 
	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_MASTER_ENABLE, 0,
											&status));
	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_PARALLEL_SERIES, 0,
											&parallelSeries));
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_SLAVE_NUMBERS, 0,
											&slaveNumbers));
	
	if ( 1 ) // increase rating according slave numbers and parallel/series status
		{
			if ( parallelSeries==VI_TRUE || status==VI_FALSE) // in series mode, increase voltage rating
				{
					if ( status==VI_TRUE )
						nums=((ViReal64)(slaveNumbers+1));
					else
						nums=1;
					checkErr( Ivi_GetViReal64EntryFromIndex (0, &attrVoltageRangeTable, &vireal64_min, &vireal64_max, VI_NULL, VI_NULL, VI_NULL));
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, vireal64_max*nums);
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, vireal64_min*nums);
					checkErr( Ivi_GetViReal64EntryFromIndex (0, &attrVoltSlewRangeTable, &vireal64_min, &vireal64_max, VI_NULL, VI_NULL, VI_NULL));
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MAX, 0, vireal64_max*nums);
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MIN, 0, vireal64_min*nums);
					checkErr( Ivi_GetViReal64EntryFromIndex (0, &attrVdcOnRiseRangeTable, &vireal64_min, &vireal64_max, VI_NULL, VI_NULL, VI_NULL));
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_R_MAX, 0, vireal64_max*nums);
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_F_MIN, 0, vireal64_min*nums);
				}
			if ( parallelSeries==VI_FALSE || status==VI_FALSE) // in parallel mode, increase current rating
				{
					if ( status==VI_TRUE )
						nums=((ViReal64)(slaveNumbers+1));
					else
						nums=1;
					checkErr( Ivi_GetViReal64EntryFromIndex (0, &attrCurrentRangeTable, &vireal64_min, &vireal64_max, VI_NULL, VI_NULL, VI_NULL));
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MAX, 0, vireal64_max*nums);
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MIN, 0, vireal64_min*nums);
					checkErr( Ivi_GetViReal64EntryFromIndex (0, &attrCurrSlewRangeTable, &vireal64_min, &vireal64_max, VI_NULL, VI_NULL, VI_NULL));
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_ISLEW_MAX, 0, vireal64_max*nums);
					Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_ISLEW_MIN, 0, vireal64_min*nums);
				}
			
			if ( status==VI_TRUE )
					nums=((ViReal64)(slaveNumbers+1));
			else
					nums=1;	
			checkErr( Ivi_GetViReal64EntryFromIndex (0, &attrPowerProtectRangeTable, &vireal64_min, &vireal64_max, VI_NULL, VI_NULL, VI_NULL));
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PWR_MAX, 0, vireal64_max*nums);
			Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PWR_MIN, 0, vireal64_min*nums);
		}
	}
	
Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_SwapAttributeViReal64MinMax1
 * Purpose:  Swap Attribute's Min&Max
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SwapAttributeViReal64MinMax1 (ViSession vi, ViAttr attributeId, ViReal64 min, ViReal64 max, IviRangeTablePtr *rangeTablePtr)
{
	ViStatus    error = VI_SUCCESS;   
	IviRangeTablePtr	tblPtr=VI_NULL;
	ViReal64	org_min=-1,org_max=-1;

	checkErr( Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));
	checkErr( Ivi_GetViReal64EntryFromIndex (0, tblPtr, &org_min, &org_max, VI_NULL, VI_NULL, VI_NULL));
	if ( min==Do_not_write )
		checkErr( Ivi_SetRangeTableEntry (tblPtr, 0, org_min, max, VI_NULL, VI_NULL, VI_NULL));
	else if ( max==Do_not_write )		
		checkErr( Ivi_SetRangeTableEntry (tblPtr, 0, min, org_max, VI_NULL, VI_NULL, VI_NULL));  		
	else	
		checkErr( Ivi_SetRangeTableEntry (tblPtr, 0, min, max, VI_NULL, VI_NULL, VI_NULL));  
	//checkErr( Ivi_SetStoredRangeTablePtr (vi, attributeId, tblPtr));	

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

/*****************************************************************************
 * Function: chr62000_Digit_boundary   
 * Purpose:  specify valid decimal place for specified value
 * support only -- valid value:+-10730000, valid digit: 1 & 2
 * accuracy to the sixth decimal place
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Digit_boundary (ViReal64 *value, ViInt32 digit)
{
	ViStatus    error = VI_SUCCESS;   
	ViInt32		intvalue=1;
	
	*value=*value+0.000001; // if *value=0.999999 then *value=1; 
	
	if (*value<10730000 && *value>-10730000 && digit<=2 && digit>=1)
	{
		ViInt32	mult=1;
		if (digit==1)
			mult=10;
		else
			mult=100;
		*value=(*value)*mult;
		intvalue=*value;	   
		*value=((ViReal64)(intvalue))/mult;
	}

Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_Detect_error   
 * Purpose:  specify valid decimal place for specified value
 * support only -- valid value:+-10730000, valid digit: 1 & 2
 * accuracy to the sixth decimal place
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Detect_error (ViSession vi, ViChar Cmd[])
{
	ViStatus    error = VI_SUCCESS;   
	ViChar		rdBuffer[BUFFER_SIZE];
	ViInt32		errCode=256; 
	
	checkErr( chr62000_error_query (vi, &errCode, rdBuffer));
	
	if (errCode!=0)
	{
		ViSession   io = Ivi_IOSession(vi); 
		
		viCheckErr( viPrintf (io, "%s", Cmd));
		Delay (delay_time);
		checkErr( error= CHR62000_ERROR_EXECUTION ); 
	}
	
Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_MST_APG_PRG_status   
 * Purpose:  This function detect master control enable, APG, program running
 *			 , and slave state to prevent unacceptable operation
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_MST_APG_PRG_status (ViSession vi, ViInt32 condition)
{
	ViStatus    error = VI_SUCCESS;
	ViBoolean	status = VI_TRUE;
	ViInt32		apg_status=-1,MSTSLV_ID=-1;
	ViChar		programRunningState[20]; 
	
	switch (condition)
	{
		case 0: //if Master control enabled, then error
				checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_MASTER_ENABLE, 0,
											&status));
				if ( status == VI_TRUE) 	
					checkErr( error=CHR62000_ERROR_MASTER_CONTROLLING );
				break;
		case 1: //if APG control enabled, then error
				checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_APG_MODE, 0,
											&apg_status));
				if ( apg_status != 0) 	
					checkErr( error=CHR62000_ERROR_APG_CONTROLLING );
				break;											
		case 2: //if Program is running, then error
				if (!Ivi_Simulating(vi))
				{
					ViSession   io = Ivi_IOSession(vi);
					viCheckErr( viPrintf (io, "PROG:RUN?\n"));
					Delay (delay_time);
					viCheckErr( viScanf (io, "%s", programRunningState)); 
					Delay (delay_time);
				}
				else
				{
					checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_PROG_RUN, 0,
												&status));
					if (status == VI_TRUE)
					Fmt (programRunningState, ON_STRING);
					else if (status == VI_FALSE)
					Fmt (programRunningState, OFF_STRING);
				}
				if(stricmp(programRunningState,ON_STRING) == 0)
					checkErr( error=CHR62000_ERROR_PROGRAM_CONTROLLING ); 
				break;
		case 3:	// command reject when both Master control enabled and in slave mode
				checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_MASTER_ENABLE, 0,
											&status));
				checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_MSTSLV_ID, 0,
											&MSTSLV_ID));
				if ( (status == VI_TRUE) && (MSTSLV_ID != 0) )											
					checkErr( error=CHR62000_ERROR_SETTING_CONFLICT );
				break;
		case 4:	// command reject when in slave mode
				checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_MSTSLV_ID, 0,
											&MSTSLV_ID));
				if ( MSTSLV_ID != 0 )											
					checkErr( error=CHR62000_ERROR_SETTING_CONFLICT );
				break;
		default:
				break;
	}
	
Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_Invalidate_Program   
 * Purpose:  Invalidate some attributes of program when the user clear program
 *           parameter
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Invalidate_Program (ViSession vi, ViInt32 programSelected)
{
	ViStatus    error = VI_SUCCESS;
    ViChar		ChannelName[10];
    ViInt32		i=0,nCount=0,index=0;
	
    /* ChannelName counting and invalidate channel-base attribute */ 
    index= (programSelected-1)*10+1;
    if (!Ivi_Simulating(vi)) 
    {
    	Fmt (ChannelName,"%d", programSelected); 
		checkErr( Ivi_InvalidateAttribute (vi, ChannelName, CHR62000_ATTR_PROG_LINK));
		checkErr( Ivi_InvalidateAttribute (vi, ChannelName, CHR62000_ATTR_PROG_COUNT));
    	for(i=index;i<index+10;i++)
		{
			Fmt (ChannelName,"%d",i);
			checkErr( Ivi_InvalidateAttribute (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_TYPE)); 
			checkErr( Ivi_InvalidateAttribute (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_VOLTAGE)); 
			checkErr( Ivi_InvalidateAttribute (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_VOLT_SLEW)); 
			checkErr( Ivi_InvalidateAttribute (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_CURRENT)); 
			checkErr( Ivi_InvalidateAttribute (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_CURR_SLEW)); 
			checkErr( Ivi_InvalidateAttribute (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_TTL)); 
			checkErr( Ivi_InvalidateAttribute (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_TIME)); 
		}
	}	
	else // Simulating
	{
		Fmt (ChannelName,"%d", programSelected);
		checkErr( Ivi_SetAttributeViInt32 (vi, ChannelName, CHR62000_ATTR_PROG_LINK, 
											0, 0));
		checkErr( Ivi_SetAttributeViInt32 (vi, ChannelName, CHR62000_ATTR_PROG_COUNT,
											0, 1));
   		for(i=index;i<index+10;i++)
		{
			Fmt (ChannelName,"%d",i);
			checkErr( Ivi_SetAttributeViInt32 (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_TYPE, 0, 0)); 
			checkErr( Ivi_SetAttributeViReal64 (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_VOLTAGE, 0, 0.00)); 
			checkErr( Ivi_SetAttributeViReal64 (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_VOLT_SLEW, 0, 10.00)); 
			checkErr( Ivi_SetAttributeViReal64 (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_CURRENT, 0, 0.00)); 
			checkErr( Ivi_SetAttributeViReal64 (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_CURR_SLEW, 0, 1.00)); 
			checkErr( Ivi_SetAttributeViInt32 (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_TTL, 0, 0)); 
			checkErr( Ivi_SetAttributeViReal64 (vi, ChannelName, CHR62000_ATTR_PROG_SEQ_TIME, 0, 0.00)); 
		}		
	}
	
Error:
	return error;
}
                                                                       
/*****************************************************************************
 *------------ User-Callable Functions (Exportable Functions) ---------------*
 *****************************************************************************/

/*****************************************************************************
 * Function: chr62000_init   
 * Purpose:  VXIplug&play required function.  Calls the   
 *           chr62000_InitWithOptions function.   
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_init (ViRsrc resourceName, ViBoolean IDQuery,
                                 ViBoolean resetDevice, ViSession *newVi)
{   
    ViStatus    error = VI_SUCCESS;

    if (newVi == VI_NULL)
        {
        Ivi_SetErrorInfo (VI_NULL, VI_FALSE, IVI_ERROR_INVALID_PARAMETER, 
                          VI_ERROR_PARAMETER4, "Null address for Instrument Handle");
        checkErr( IVI_ERROR_INVALID_PARAMETER);
        }

    checkErr( chr62000_InitWithOptions (resourceName, IDQuery, resetDevice, 
                                        "", newVi));

Error:  
    return error;
}

/*****************************************************************************
 * Function: chr62000_InitInterface   
 * Purpose:  A function which can initial the driver with GPIB or RS-232.  
 *           Calls the chr62000_InitWithOptions function.   
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_InitInterface (ViRsrc resourceName, ViBoolean IDQuery,
                                          ViBoolean resetDevice,
                                          ViInt32 baudRate, ViChar IDString[],
                                          ViChar modelNumber[],
                                          ViSession *newVi)
{
  	ViStatus    error = VI_SUCCESS;
    ViSession   vi = VI_NULL;

    g_nBaudRate=baudRate;
    
    if (newVi == VI_NULL)
        {
        Ivi_SetErrorInfo (VI_NULL, VI_FALSE, IVI_ERROR_INVALID_PARAMETER, 
                          VI_ERROR_PARAMETER4, "Null address for Instrument Handle");
        checkErr( IVI_ERROR_INVALID_PARAMETER);
        }
	if (IDString == VI_NULL)
		viCheckParm( IVI_ERROR_INVALID_PARAMETER, 5, "Null address for ID String" );
	if (modelNumber == VI_NULL)
		viCheckParm( IVI_ERROR_INVALID_PARAMETER, 6, "Null address for Model Number" );
	        
    checkErr( chr62000_InitWithOptions (resourceName, IDQuery, resetDevice, 
                                        "", newVi));
    vi=*newVi; 
	checkErr( Ivi_GetAttributeViString (vi, VI_NULL, CHR62000_ATTR_ID_QUERY_RESPONSE, 
                                        0, BUFFER_SIZE, IDString));
	sscanf (IDString, "%*[^,],%256[^,]", modelNumber);
    
Error:  
    return error;
}

/*****************************************************************************
 * Function: chr62000_InitWithOptions                                       
 * Purpose:  This function creates a new IVI session and calls the 
 *           IviInit function.                                     
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_InitWithOptions (ViRsrc resourceName, ViBoolean IDQuery,
                                            ViBoolean resetDevice, ViString optionString, 
                                            ViSession *newVi)
{   
    ViStatus    error = VI_SUCCESS;
    ViSession   vi = VI_NULL;
    ViChar      newResourceName[IVI_MAX_MESSAGE_BUF_SIZE];
    ViChar      newOptionString[IVI_MAX_MESSAGE_BUF_SIZE];
    ViBoolean   isLogicalName;
    
    if (newVi == VI_NULL)
        {
        Ivi_SetErrorInfo (VI_NULL, VI_FALSE, IVI_ERROR_INVALID_PARAMETER, 
                          VI_ERROR_PARAMETER5, "Null address for Instrument Handle");
        checkErr( IVI_ERROR_INVALID_PARAMETER);
        }

    *newVi = VI_NULL;
    
    checkErr( Ivi_GetInfoFromResourceName (resourceName, optionString, newResourceName,
                                           newOptionString, &isLogicalName));
    
        /* create a new interchangeable driver */
    checkErr( Ivi_SpecificDriverNew ("chr62000", newOptionString, &vi));  
    
        /* init the driver */
    checkErr( chr62000_IviInit (newResourceName, IDQuery, resetDevice, vi)); 
    if (isLogicalName)	  
        checkErr( Ivi_ApplyDefaultSetup (vi));
    *newVi = vi;
    
Error:
    if (error < VI_SUCCESS) 
        Ivi_Dispose (vi);
        
    return error;
}

/*****************************************************************************
 * Function: chr62000_IviInit                                                       
 * Purpose:  This function is called by chr62000_InitWithOptions  
 *           or by an IVI class driver.  This function initializes the I/O 
 *           interface, optionally resets the device, optionally performs an
 *           ID query, and sends a default setup to the instrument.                
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_IviInit (ViRsrc resourceName, ViBoolean IDQuery,
                                    ViBoolean reset, ViSession vi)
{
    ViStatus    error = VI_SUCCESS;
    ViSession   io = VI_NULL;
    ViChar		Expand_ChannelName[BUFFER_SIZE];
    ViChar		RSCName[BUFFER_SIZE]="";
    ViInt32		i=0,nCount=0;
    
    /* Expand Channel to MaxChannel */ 
    for(i=1;i<=MaxChannel;i++)
	{
		if (i==MaxChannel)
			nCount+= sprintf(Expand_ChannelName+nCount,"%d",i);			
		else
			nCount+= sprintf(Expand_ChannelName+nCount,"%d,",i); 
	}
    checkErr( Ivi_BuildChannelTable (vi, Expand_ChannelName, VI_FALSE, VI_NULL));
   
     /* Add attributes */
    checkErr( chr62000_InitAttributes (vi));
    
	if ( !Ivi_Simulating(vi) )		  
	{
	    ViSession   rmSession = VI_NULL;
	    ViUInt16	INTF_TYPE=0;
	    ViChar		SOCKET_STR[12]="SOCKET";
	    char		*RTN_STR;

	        /* Open instrument session */
	    checkErr( Ivi_GetAttributeViSession (vi, VI_NULL, IVI_ATTR_VISA_RM_SESSION, 0,
	                                       		&rmSession)); 
	    viCheckErr( viOpen (rmSession, resourceName, VI_NULL, VI_NULL, &io));
	        /* io session owned by driver now */
	    checkErr( Ivi_SetAttributeViSession (vi, VI_NULL, IVI_ATTR_IO_SESSION, 0, io));  

		sprintf (RSCName, "%s", resourceName);
		StringUpperCase (RSCName);
		if ( (strncmp(RSCName,"ASRL",4)==0) || (strncmp(RSCName,"USB",3)==0) || (strncmp(RSCName,"TCPIP",5)==0) )
		{
			//Configure VISA Formatted I/O 
			viCheckErr( viSetAttribute (io, VI_ATTR_TERMCHAR, 0x0A)); 
			viCheckErr( viSetAttribute (io, VI_ATTR_TERMCHAR_EN, VI_TRUE));
			
			if ( strncmp(RSCName,"ASRL",4)==0 )
			{
			viCheckErr( viFlush (io, VI_ASRL_IN_BUF|VI_ASRL_OUT_BUF|VI_WRITE_BUF_DISCARD|VI_READ_BUF_DISCARD));
			viCheckErr( viSetBuf (io, VI_ASRL_IN_BUF | VI_ASRL_OUT_BUF, 512));
			viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_END_IN, VI_ASRL_END_TERMCHAR)); // VI_ASRL_END_NONE or VI_ASRL_END_TERMCHAR
			// viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_END_OUT, VI_ASRL_END_TERMCHAR)); // VI_ASRL_END_NONE or VI_ASRL_END_TERMCHAR
			viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_FLOW_CNTRL, VI_ASRL_FLOW_NONE));
			viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_BAUD, g_nBaudRate));
			viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_DATA_BITS, 8));
			viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_PARITY,0));
			viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_STOP_BITS,10));
			}
			
			viGetAttribute (io, VI_ATTR_INTF_TYPE, &INTF_TYPE);
			if (INTF_TYPE==6)
			{
				RTN_STR=strstr (RSCName,SOCKET_STR);
				
				if ( RTN_STR != NULL )
				{
					viSetAttribute (io, VI_ATTR_SUPPRESS_END_EN, VI_TRUE);
					//If this attribute is set to VI_FALSE, if NI-VISA reads some data and then detects a pause in the arrival of data packets, it will terminate the read operation.
				}
			}
		}
		
			/* Configure VISA Formatted I/O */
		viCheckErr( viSetAttribute (io, VI_ATTR_TMO_VALUE, normal_timeout_5000 ));
		viCheckErr( viSetBuf (io, VI_READ_BUF | VI_WRITE_BUF, 10000));
		viCheckErr( viSetAttribute (io, VI_ATTR_WR_BUF_OPER_MODE, VI_FLUSH_ON_ACCESS));
		viCheckErr( viSetAttribute (io, VI_ATTR_RD_BUF_OPER_MODE, VI_FLUSH_ON_ACCESS));
	}

    /*- Reset instrument ----------------------------------------------------*/
    if (reset) 
        checkErr( chr62000_reset (vi));
    else  /*- Send Default Instrument Setup ---------------------------------*/
        checkErr( chr62000_DefaultInstrSetup (vi));
	
	/*- Identification Query ------------------------------------------------*/
	if (IDQuery)                               
	    {
	    ViChar rdBuffer[BUFFER_SIZE];
	
	    #define VALID_RESPONSE_STRING_06P_100_25        "CHROMA,62006P-100-25"
	    #define VALID_RESPONSE_STRING_06P_300_8         "CHROMA,62006P-300-8"
	    #define VALID_RESPONSE_STRING_12P_30_160        "CHROMA,62012P-30-160"
	    #define VALID_RESPONSE_STRING_12P_80_60         "CHROMA,62012P-80-60"
	    #define VALID_RESPONSE_STRING_12P_100_50        "CHROMA,62012P-100-50"
	    #define VALID_RESPONSE_STRING_12P_600_8         "CHROMA,62012P-600-8"
	    #define VALID_RESPONSE_STRING_24P_80_60         "CHROMA,62024P-80-60"
	    
	    #define VALID_RESPONSE_STRING_06P_30_80         "CHROMA,62006P-30-80"
		#define VALID_RESPONSE_STRING_12P_40_120        "CHROMA,62012P-40-120"
		#define VALID_RESPONSE_STRING_24P_40_120        "CHROMA,62024P-40-120"
		#define VALID_RESPONSE_STRING_24P_100_50        "CHROMA,62024P-100-50"
		#define VALID_RESPONSE_STRING_24P_600_8         "CHROMA,62024P-600-8"
		#define VALID_RESPONSE_STRING_50P_100_100       "CHROMA,62050P-100-100"
	    
	
	    checkErr( Ivi_GetAttributeViString (vi, VI_NULL, CHR62000_ATTR_ID_QUERY_RESPONSE, 
	                                        0, BUFFER_SIZE, rdBuffer));
	    
	    if (strncmp (rdBuffer, VALID_RESPONSE_STRING_06P_100_25, strlen(VALID_RESPONSE_STRING_06P_100_25)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_06P_300_8, strlen(VALID_RESPONSE_STRING_06P_300_8)) == 0)
	    {}
	 	else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_12P_30_160, strlen(VALID_RESPONSE_STRING_12P_30_160)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_12P_80_60, strlen(VALID_RESPONSE_STRING_12P_80_60)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_12P_100_50, strlen(VALID_RESPONSE_STRING_12P_100_50)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_12P_600_8, strlen(VALID_RESPONSE_STRING_12P_600_8)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_24P_80_60, strlen(VALID_RESPONSE_STRING_24P_80_60)) == 0)
	    {}   
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_06P_30_80, strlen(VALID_RESPONSE_STRING_06P_30_80)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_12P_40_120, strlen(VALID_RESPONSE_STRING_12P_40_120)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_24P_40_120, strlen(VALID_RESPONSE_STRING_24P_40_120)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_24P_100_50, strlen(VALID_RESPONSE_STRING_24P_100_50)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_24P_600_8, strlen(VALID_RESPONSE_STRING_24P_600_8)) == 0)
	    {}
	    else if (strncmp (rdBuffer, VALID_RESPONSE_STRING_50P_100_100, strlen(VALID_RESPONSE_STRING_50P_100_100)) == 0)
	    {}
	    else viCheckErr( VI_ERROR_FAIL_ID_QUERY);
	    
	    }
	
	checkErr( chr62000_MstSlv_Status (vi)); // detect master/slave status
    checkErr( chr62000_CheckStatus (vi));

Error:
    if (error < VI_SUCCESS)
        {
        if (!Ivi_Simulating (vi) && io)
            viClose (io);
        }
    return error;
}

/*****************************************************************************
 * Function: chr62000_close                                                           
 * Purpose:  This function closes the instrument.                            
 *
 *           Note:  This function must unlock the session before calling
 *           Ivi_Dispose.
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_close (ViSession vi)
{
    ViStatus    error = VI_SUCCESS;
    
    checkErr( Ivi_LockSession (vi, VI_NULL));
    
    checkErr( chr62000_IviClose (vi));

Error:    
    Ivi_UnlockSession (vi, VI_NULL);
    Ivi_Dispose (vi);  

    return error;
}

/*****************************************************************************
 * Function: chr62000_IviClose                                                        
 * Purpose:  This function performs all of the drivers clean-up operations   
 *           except for closing the IVI session.  This function is called by 
 *           chr62000_close or by an IVI class driver. 
 *
 *           Note:  This function must close the I/O session and set 
 *           IVI_ATTR_IO_SESSION to 0.
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_IviClose (ViSession vi)
{
    ViStatus error = VI_SUCCESS;
    ViSession io = VI_NULL;

        /* Do not lock here.  The caller manages the lock. */

    checkErr( Ivi_GetAttributeViSession (vi, VI_NULL, IVI_ATTR_IO_SESSION, 0, &io));

Error:
    Ivi_SetAttributeViSession (vi, VI_NULL, IVI_ATTR_IO_SESSION, 0, VI_NULL);
    if(io)                                                      
        {
        viClose (io);
        }
    return error;   
}

/*****************************************************************************
 * Function: chr62000_Measure   
 * Purpose:  This function measure the voltage | current | power of the 
 *           instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Measure (ViSession vi, ViInt32 measureType,
                                 ViReal64 *readValue)
{
	ViStatus	error = VI_SUCCESS;
	ViString	MeasCmd;
	
	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	
	if (readValue == VI_NULL)
		viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for ReadValue" );
		
	if (!Ivi_Simulating(vi))
    {					  
		ViSession   io = Ivi_IOSession(vi);
		checkErr( Ivi_GetViInt32EntryFromIndex (measureType, &attrMeasTypeRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &MeasCmd, VI_NULL));
		viCheckErr( viPrintf (io, "MEAS:%s\n", MeasCmd));
		Delay (delay_time);
		viCheckErr( viScanf (io, "%Lf", readValue));
		Delay (delay_time);
	}
	else //simulation
		chr62000_Simulate_Meas_Fetch (vi, measureType, readValue);
		
	checkErr( chr62000_CheckStatus (vi));
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_Fetch   
 * Purpose:  This function fetch the voltage | current | power of the 
 *           instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Fetch (ViSession vi, ViInt32 fetchType,
                                  ViReal64 *readValue)
{
	ViStatus	error = VI_SUCCESS;
	ViString	FetchCmd;
	
	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	
	if (readValue == VI_NULL)
		viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for ReadValue" );
		
	if (!Ivi_Simulating(vi))
    {
		ViSession   io = Ivi_IOSession(vi);
		checkErr( Ivi_GetViInt32EntryFromIndex (fetchType, &attrFetchTypeRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &FetchCmd, VI_NULL));
		viCheckErr( viPrintf (io, "FETC:%s\n", FetchCmd));
		Delay (delay_time);
		viCheckErr( viScanf (io, "%Lf", readValue));
		Delay (delay_time);
	}
	else //simulation
		chr62000_Simulate_Meas_Fetch (vi, fetchType, readValue);  

	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
	
}

/*****************************************************************************
 * Function: chr62000_FetchStatus   
 * Purpose:  This function query the state of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_FetchStatus (ViSession vi,
                                        ViChar Status[])
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (Status == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Status");
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viPrintf (io, "FETC:STAT?\n"));
		Delay (delay_time);
		viCheckErr( viScanf (io, "%s", Status)); 
		Delay (delay_time);
	}
	else
		Fmt (Status, "0");

	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryBeeper   
 * Purpose:  This function query the beeper state of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryBeeper (ViSession vi,
                                        ViChar beeper[])
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean	get_status = VI_TRUE;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	if (beeper == VI_NULL)
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Beeper");
	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_BEEPER, 0,
											&get_status));
	if (get_status == VI_TRUE)
		Fmt (beeper, ON_STRING);
	else if (get_status == VI_FALSE)
		Fmt (beeper, OFF_STRING);
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryOutput   
 * Purpose:  This function query the output state of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryOutput (ViSession vi,
                                        ViChar output[])
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean	get_status = VI_TRUE;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	if (output == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Output");

	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viPrintf (io, "CONF:OUTP?\n"));
		Delay (delay_time);
		viCheckErr( viScanf (io, "%s", output)); 
		Delay (delay_time);
	}
	else
	{
		checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_OUTPUT, 0,
												&get_status));
		if (get_status == VI_TRUE)
			Fmt (output, ON_STRING);
		else if (get_status == VI_FALSE)
			Fmt (output, OFF_STRING);
	}
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryFoldback   
 * Purpose:  This function query the foldback state and foldback delay time
 *           of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryFoldback (ViSession vi,
                                          ViChar foldback[],
                                          ViReal64 *foldbackDelayTime)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	ViInt32		get_status = 0;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (foldback == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Foldback");
    if (foldbackDelayTime == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Foldback Delay Time");
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_FOLDBACK_PROTECT, 0,
											&get_status));
											
	checkErr( Ivi_GetViInt32EntryFromIndex (get_status, &attrFoldbackProtectRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));											
	Fmt	(foldback, "%s", Cmd);	
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_FOLDBACK_DELAY_TIME, IVI_VAL_DIRECT_USER_CALL,
											foldbackDelayTime));
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryAPGMode   
 * Purpose:  This function query the APG state and APG reference voltage
 *           of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryAPGMode (ViSession vi,
                                         ViChar APGMode[],
                                         ViChar APGReferenceVoltage[])
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	ViInt32		get_status = 0;
	ViBoolean	ref_volt = VI_FALSE;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (APGMode == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for APG Mode");
    if (APGReferenceVoltage == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for APG Reference Voltage");
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_APG_MODE, 0,
											&get_status));
	checkErr( Ivi_GetViInt32EntryFromIndex (get_status, &attrApgModeRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));											
	Fmt	(APGMode, "%s", Cmd);
	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_APG_REFERENCE_VOLTAGE, 0,
											&ref_volt));	
	if (ref_volt == VI_FALSE)
		Fmt	(APGReferenceVoltage, "5V"); 
	else if (ref_volt == VI_TRUE)
		Fmt	(APGReferenceVoltage, "10V"); 		
		
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryProgPara   
 * Purpose:  This function query the whole program parameter of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryProgPara (ViSession vi,
                                          ViInt32 programSelected,
                                          ViInt32 sequenceSelected,
                                          ViInt32 *link, ViInt32 *count,
                                          ViChar type[], ViReal64 *voltage,
                                          ViReal64 *voltageSlew,
                                          ViReal64 *current,
                                          ViReal64 *currentSlew, ViInt32 *TTL,
                                          ViReal64 *time)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		channel[10];
	ViInt32		value=0;
	ViReal64	value1=0;
	
	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	checkErr( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_PROG_SELECTED,
										IVI_VAL_DIRECT_USER_CALL, programSelected));
	checkErr( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_PROG_SEQ_SELECTED,
										IVI_VAL_DIRECT_USER_CALL , sequenceSelected));  			    //modify by justin.hsu 12/03/08 (1-10 -> 1-100)
	Fmt (channel,"%d", programSelected); // The channel of the link and count is equal program
	if (link != VI_NULL)
	checkErr( Ivi_GetAttributeViInt32 (vi, channel, CHR62000_ATTR_PROG_LINK, 0,
											link));
	if (count != VI_NULL)
	checkErr( Ivi_GetAttributeViInt32 (vi, channel, CHR62000_ATTR_PROG_COUNT, 0,
											count));											
	Fmt (channel,"%d", sequenceSelected); //	calculate channel for specific program and sequence		//modify by justin.hsu 12/03/08 									
	if (type != VI_NULL)									
	{
		checkErr( Ivi_GetAttributeViInt32 (vi, channel, CHR62000_ATTR_PROG_SEQ_TYPE, 0,
												&value));
		if (value == 0)	
			Fmt (type, "AUTO");
		else if (value == 1)	
			Fmt (type, "MANUAL");
		else if (value == 2)	
			Fmt (type, "EXT.TRIGGER");
		else if (value == 3)	
			Fmt (type, "SKIP");			
	}		
	if (voltage != VI_NULL)											
	checkErr( Ivi_GetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_VOLTAGE, 0,
											voltage));
	if (voltageSlew != VI_NULL)											
	checkErr( Ivi_GetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_VOLT_SLEW, 0,
											voltageSlew));
	if (current != VI_NULL)												
	checkErr( Ivi_GetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_CURRENT, 0,
											current));
	if (currentSlew != VI_NULL)											
	{
		checkErr( Ivi_GetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_CURR_SLEW, 0,
												&value1));
		if ( value1==prog_curr_slew_inf )
			*currentSlew=atof (ASCII_INF);
		else
			*currentSlew=value1;
	}											
	if (TTL != VI_NULL)												
	checkErr( Ivi_GetAttributeViInt32 (vi, channel, CHR62000_ATTR_PROG_SEQ_TTL, 0,
											TTL));
	if (time != VI_NULL)
	{
		if (value == 0)	// if sequence type is auto, then the time is meaningful, else retrun time=-1
			checkErr( Ivi_GetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_TIME, 0,
													time));
		else
			*time=-1;
	}					   
	checkErr( chr62000_CheckStatus (vi));
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryProgRunState   
 * Purpose:  This function query the program running state of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryProgRunState (ViSession vi,
                                              ViChar programRunningState[])
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean	get_status = VI_TRUE;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (programRunningState == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Program Running State");
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viPrintf (io, "PROG:RUN?\n"));
		Delay (delay_time);
		viCheckErr( viScanf (io, "%s", programRunningState)); 
		Delay (delay_time);
	}
	else
	{
		checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_PROG_RUN, 0,
												&get_status));
		if (get_status == VI_TRUE)
			Fmt (programRunningState, ON_STRING);
		else if (get_status == VI_FALSE)
			Fmt (programRunningState, OFF_STRING);
	}
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryMstSlvID   
 * Purpose:  This function query the Master/Slave ID of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryMstSlvID (ViSession vi,
                                          ViChar ID[])
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	ViInt32		MSTSLV_ID=-1;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	if (ID == VI_NULL)
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for ID");
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_MSTSLV_ID, 0,
								&MSTSLV_ID));
	checkErr( Ivi_GetViInt32EntryFromIndex (MSTSLV_ID, &attrMstslvIdRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	Fmt	(ID, "%s", Cmd);												
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryMstPara   
 * Purpose:  This function query the parallel/series state and slave numbers 
 *  		 of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryMstPara (ViSession vi,
                                         ViChar parallelSeries[],
                                         ViInt32 *slaveNumbers)
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean	get_status = VI_TRUE;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (parallelSeries == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Parallel Series");
    if (slaveNumbers == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Slave Numbers");
	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_PARALLEL_SERIES, 0,
											&get_status));
	if ( get_status==VI_TRUE )										
		Fmt	(parallelSeries, "SERIES");		
	else
		Fmt	(parallelSeries, "PARALLEL");	
		
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_SLAVE_NUMBERS, 0,
											slaveNumbers));
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryMstEnbState   
 * Purpose:  This function query the master control enable state of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryMstEnbState (ViSession vi,
                                             ViChar masterEnableState[])
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean	get_status = VI_TRUE;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (masterEnableState == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Master Enable State");
	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_MASTER_ENABLE, 0,
											&get_status));
	if ( get_status==VI_TRUE )										
		Fmt	(masterEnableState, ON_STRING);		
	else
		Fmt	(masterEnableState, OFF_STRING);	
		
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryBacklight   
 * Purpose:  This function query the backlight of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryBacklight (ViSession vi,
                                           ViChar backlight[])
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	ViInt32		get_status = 0;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (backlight == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Backlight");
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_BACKLIGHT, 0,
											&get_status));
	checkErr( Ivi_GetViInt32EntryFromIndex (get_status, &attrBacklightRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));											
	Fmt	(backlight, "%s", Cmd);
		
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryRmtInhibit   
 * Purpose:  This function query the remote inhibit state of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryRmtInhibit (ViSession vi,
                                            ViChar remoteInhibit[])                                     
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		Inh_Status = 0;
	ViString	Cmd;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	if (remoteInhibit == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Remote Inhibit");

	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viPrintf (io, "CONF:INH?\n"));
		Delay (delay_time);
		viCheckErr( viScanf (io, "%s", remoteInhibit)); 
		Delay (delay_time);
	}
	else
	{
		checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_INHIBIT, 0,
											&Inh_Status));
		checkErr( Ivi_GetViInt32EntryFromIndex (Inh_Status, &attrInhibitRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
		Fmt (remoteInhibit, Cmd);
	}
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryProgMode   
 * Purpose:  This function query the program mode of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryProgMode (ViSession vi,
                                          ViChar programMode[])
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	ViInt32		get_status = 0;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (programMode == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Program Mode");
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_PROG_MODE, 0,
											&get_status));
	checkErr( Ivi_GetViInt32EntryFromIndex (get_status, &attrProgModeRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));											
	Fmt	(programMode, "%s", Cmd);
		
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryESR   
 * Purpose:  This function query the standard event status register of the 
 *           instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryESR (ViSession vi,
                                     ViInt32 *statusRegister)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (statusRegister == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Status Register");
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viPrintf (io, "*ESR?\n"));
		Delay (delay_time);
		viCheckErr( viScanf (io, "%d", statusRegister)); 
		Delay (delay_time);
	}
	else
		*statusRegister=0;

	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QuerySTB   
 * Purpose:  This function query the read status byte of the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QuerySTB (ViSession vi,
                                     ViInt32 *statusByte)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (statusByte == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Status Byte");
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viPrintf (io, "*STB?\n"));
		Delay (delay_time);
		viCheckErr( viScanf (io, "%d", statusByte)); 
		Delay (delay_time);
	}
	else
		*statusByte=0;

	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_QueryOPC   
 * Purpose:  This function return 1 when all pending operations are completed
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_QueryOPC (ViSession vi,
                                     ViInt32 *operationComplete)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));
    if (operationComplete == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Operation Complete");
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viPrintf (io, "*OPC?\n"));
		Delay (delay_time);
		viCheckErr( viScanf (io, "%d", operationComplete)); 
		Delay (delay_time);
	}
	else
		*operationComplete=1;

	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_reset                                                         
 * Purpose:  This function resets the instrument.                          
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_reset (ViSession vi)
{
    ViStatus    error = VI_SUCCESS;

    checkErr( Ivi_LockSession (vi, VI_NULL));
    
    checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	if (!Ivi_Simulating(vi))                /* call only when locked */
	    {
	    ViSession   io = Ivi_IOSession(vi); /* call only when locked */
	    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));
	    viCheckErr( viPrintf (io, "*RST\n"));
	    Delay (1);
	    }
	
	checkErr( chr62000_DefaultInstrSetup (vi));                                
	
	checkErr( chr62000_CheckStatus (vi));                                      

Error:
    Ivi_UnlockSession (vi, VI_NULL);
    return error;
}

/*****************************************************************************
 * Function: chr62000_self_test                                                       
 * Purpose:  This function executes the instrument self-test and returns the 
 *           result.                                                         
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_self_test (ViSession vi, ViInt16 *testResult, 
                                      ViChar testMessage[])
{
    ViStatus    error = VI_SUCCESS;

    checkErr( Ivi_LockSession (vi, VI_NULL));

    if (testResult == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Test Result");
    if (testMessage == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Test Message");

	viCheckWarn( VI_WARN_NSUP_SELF_TEST);

Error:
    Ivi_UnlockSession(vi, VI_NULL);
    return error;
}

/*****************************************************************************
 * Function: chr62000_error_query                                                     
 * Purpose:  This function queries the instrument error queue and returns   
 *           the result.                                                     
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_error_query (ViSession vi, ViInt32 *errCode, 
                                        ViChar errMessage[])
{
    ViStatus    error = VI_SUCCESS;
    
    checkErr( Ivi_LockSession (vi, VI_NULL));
    
    if (errCode == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Error Code");
    if (errMessage == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Error Message");

	if (!Ivi_Simulating(vi))                /* call only when locked */
	    {
	    ViSession   io = Ivi_IOSession(vi); /* call only when locked */
	
	    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));
	    viCheckErr( viPrintf(io, ":SYST:ERR?\n"));
	
		viCheckErr( viScanf (io, "%ld,\"%256[^\"]", errCode, errMessage));
	
	    }
	else
	    {
	        /* Simulate Error Query */
	    *errCode = 0;
	    strcpy (errMessage, "No error");
	    }

Error:
    Ivi_UnlockSession(vi, VI_NULL);
    return error;
}

/*****************************************************************************
 * Function: chr62000_error_message                                                  
 * Purpose:  This function translates the error codes returned by this       
 *           instrument driver into user-readable strings.  
 *
 *           Note:  The caller can pass VI_NULL for the vi parameter.  This 
 *           is useful if one of the init functions fail.
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_error_message (ViSession vi, ViStatus errorCode,
                                          ViChar errorMessage[256])
{																									   
    ViStatus    error = VI_SUCCESS;
    
    static      IviStringValueTable errorTable = 
        {
			{CHR62000_ERROR_OUT_OF_RANGE,		"The value you applied is out of range."},         
			{CHR62000_ERROR_OUT_OF_VOLT_LIMIT,	"The value you applied is out of range of voltage limit."},
			{CHR62000_ERROR_OUT_OF_CURR_LIMIT,	"The value you applied is out of range of current limit."},
			{CHR62000_ERROR_OUT_OF_OVP,			"The value you applied is less than 1.1*(voltage setting)."}, 
			{CHR62000_ERROR_OUT_OF_OCP,			"The value you applied is less than 1.05*(current setting)."},
			{CHR62000_ERROR_OUT_OF_MAXPOWER,	"The value you applied is greater than max. power."}, 
			{CHR62000_ERROR_INVALID_INTERFACE,	"Invalid interface(RS232C only)."},
			{CHR62000_ERROR_INVALID_PARAMETER,  "Invalid parameter value."},
			{CHR62000_ERROR_EXECUTION,			"Execution error! Use Error-Query function to detect error."},
			{CHR62000_ERROR_MASTER_CONTROLLING,	"Setting conflict! Please disable Master control first."}, 
			{CHR62000_ERROR_APG_CONTROLLING,	"Setting conflict! Please disable APG control first."},
			{CHR62000_ERROR_PROGRAM_CONTROLLING,	"Setting conflict! Please disable Program control first."},
			{CHR62000_ERROR_SETTING_CONFLICT,	"Setting conflict."},
			{CHR62000_ERROR_OUTPUT_IS_ON,		"Setting conflict. Please disable output first."},			
            {VI_NULL,                               VI_NULL}
        };
        
    if (vi)
        Ivi_LockSession(vi, VI_NULL);

        /* all VISA and IVI error codes are handled as well as codes in the table */
    if (errorMessage == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Error Message");

    checkErr( Ivi_GetSpecificDriverStatusDesc(vi, errorCode, errorMessage, errorTable));

Error:
    if (vi)
        Ivi_UnlockSession(vi, VI_NULL);
    return error;
}

/*****************************************************************************
 * Function: chr62000_revision_query                                                  
 * Purpose:  This function returns the driver and instrument revisions.      
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_revision_query (ViSession vi, ViChar driverRev[], 
                                           ViChar instrRev[])
{
    ViStatus    error = VI_SUCCESS;
    
    checkErr( Ivi_LockSession (vi, VI_NULL));

    if (driverRev == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Driver Revision");
    if (instrRev == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Instrument Revision");

    checkErr( Ivi_GetAttributeViString (vi, VI_NULL, CHR62000_ATTR_SPECIFIC_DRIVER_REVISION, 
                                        0, 256, driverRev));
    checkErr( Ivi_GetAttributeViString (vi, "", CHR62000_ATTR_INSTRUMENT_FIRMWARE_REVISION, 
                                        0, 256, instrRev));
    checkErr( chr62000_CheckStatus (vi));

Error:    
    Ivi_UnlockSession(vi, VI_NULL);
    return error;							  
}

/*****************************************************************************
 * Function: chr62000_GetAttribute<type> Functions                                    
 * Purpose:  These functions enable the instrument driver user to get 
 *           attribute values directly.  There are typesafe versions for 
 *           ViInt32, ViReal64, ViString, ViBoolean, and ViSession attributes.                                         
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_GetAttributeViInt32 (ViSession vi, ViConstString channelName, 
                                                ViAttr attributeId, ViInt32 *value)
{                                                                                                           
    return Ivi_GetAttributeViInt32 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                    value);
}                                                                                                           
ViStatus _VI_FUNC chr62000_GetAttributeViReal64 (ViSession vi, ViConstString channelName, 
                                                 ViAttr attributeId, ViReal64 *value)
{                                                                                                           
    return Ivi_GetAttributeViReal64 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                     value);
}                                                                                                           
ViStatus _VI_FUNC chr62000_GetAttributeViString (ViSession vi, ViConstString channelName, 
                                                 ViAttr attributeId, ViInt32 bufSize, 
                                                 ViChar value[]) 
{   
    return Ivi_GetAttributeViString (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                     bufSize, value);
}   
ViStatus _VI_FUNC chr62000_GetAttributeViBoolean (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViBoolean *value)
{                                                                                                           
    return Ivi_GetAttributeViBoolean (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}                                                                                                           
ViStatus _VI_FUNC chr62000_GetAttributeViSession (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViSession *value)
{                                                                                                           
    return Ivi_GetAttributeViSession (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}                                                                                                           

/*****************************************************************************
 * Function: chr62000_SetVoltage                                       
 * Purpose:  This function set the voltage to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetVoltage (ViSession vi,
                                       ViReal64 voltage)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	volt_limit_high=0,volt_limit_low=0,curr_main=0,MaxPower=0;
	ViInt32		apg_status=-1;

	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_APG_MODE, 0, &apg_status));	
	if ( apg_status==1 || apg_status==3)
		checkErr( error=CHR62000_ERROR_APG_CONTROLLING );   
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_LIMIT_HIGH, IVI_VAL_DIRECT_USER_CALL,
											&volt_limit_high));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_LIMIT_LOW, IVI_VAL_DIRECT_USER_CALL,
											&volt_limit_low));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURRENT, IVI_VAL_DIRECT_USER_CALL,
											&curr_main));											
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_MAXPOWER, 0, &MaxPower));
	
	// if voltage > volt_limit_high or voltage < volt_limit_low, then error
	if ( ((voltage-volt_limit_high) > accuracy) || ((volt_limit_low-voltage) > accuracy) )
		checkErr( error= CHR62000_ERROR_OUT_OF_VOLT_LIMIT );
	/*else if ( (curr_main*voltage-MaxPower) > accuracy) 	// if curr_main*voltage setting bigger than MaxPower, then error 	
		checkErr( error= CHR62000_ERROR_OUT_OF_MAXPOWER );*/
		else	// the voltage setting is in voltage limit, and in MaxPower range, then write voltage setting to instrument	
			viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLTAGE, IVI_VAL_DIRECT_USER_CALL,
													voltage), 2, "Voltage");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetVoltageLimit                                       
 * Purpose:  This function set the voltage high/low limit to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetVoltageLimit (ViSession vi,
                                            ViBoolean highLow,
                                            ViReal64 voltageLimit)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	volt_main=0,volt_limit_high=0,volt_limit_low=0;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( highLow!=VI_TRUE && highLow!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
		
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));	
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLTAGE, IVI_VAL_DIRECT_USER_CALL,
											&volt_main));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_LIMIT_HIGH, IVI_VAL_DIRECT_USER_CALL,
											&volt_limit_high));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_LIMIT_LOW, IVI_VAL_DIRECT_USER_CALL,
											&volt_limit_low));											
											
	if (highLow == VI_TRUE)
	{
		if ( (volt_limit_low-voltageLimit) > accuracy )
			checkErr( error= CHR62000_ERROR_INVALID_PARAMETER );
		else
		{
			viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_LIMIT_HIGH, IVI_VAL_DIRECT_USER_CALL,
													voltageLimit), 3, "Voltage Limit");
			if ( (volt_main-voltageLimit) > accuracy )	
				checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VOLTAGE));
		}											
	}	
	
	else if (highLow == VI_FALSE)
	{
		if ( (voltageLimit-volt_limit_high) > accuracy )
			checkErr( error= CHR62000_ERROR_INVALID_PARAMETER );
		else
		{
			viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_LIMIT_LOW, IVI_VAL_DIRECT_USER_CALL,
													voltageLimit), 3, "Voltage Limit");
			if ( (voltageLimit-volt_main) > accuracy )
				checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VOLTAGE));
		}
	}	
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetVoltageProtect                                       
 * Purpose:  This function set the protect voltage to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetVoltageProtect (ViSession vi,
                                              ViReal64 voltageProtect)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	volt_main=0,min_volt_protect=0;
	ViInt32		value=0;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLTAGE, IVI_VAL_DIRECT_USER_CALL,
											&volt_main));
	value=(ViInt32)(volt_main*OVP_RATIO*1000); //the protect voltage can not less than OVP_RATIO*voltage setting
	if ( (value%100) !=0)
    {
    	value=value+100-(value%100);
    	min_volt_protect=(((ViReal64)(value))/1000);
    }
    else
    	min_volt_protect=(((ViReal64)(value))/1000);
	
	if ( (min_volt_protect-voltageProtect) > accuracy)
		checkErr( error= CHR62000_ERROR_OUT_OF_OVP );
	else		
		viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_PROTECT, IVI_VAL_DIRECT_USER_CALL,
												voltageProtect), 2, "Voltage Protect");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetVoltageSlew                                       
 * Purpose:  This function set the voltage slew rate to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetVoltageSlew (ViSession vi,
                                           ViReal64 voltageSlew)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_SLEW, IVI_VAL_DIRECT_USER_CALL,
											voltageSlew), 2, "Voltage Slew");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetDcOnVolt                                       
 * Purpose:  This function set DC_ON rising and falling voltage to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetDcOnVolt (ViSession vi,
                                        ViReal64 VDC_R, ViReal64 VDC_F)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	pre_vdc_r=0,pre_vdc_f=0;
	ViChar		output[20];
	ViBoolean	get_status = VI_TRUE;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_ON_RISE, IVI_VAL_DIRECT_USER_CALL,
											&pre_vdc_r));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_ON_FALL, IVI_VAL_DIRECT_USER_CALL,
											&pre_vdc_f));
											
	if ( ((pre_vdc_r-VDC_R) > accuracy) || ((VDC_R-pre_vdc_r) > accuracy) || ((pre_vdc_f-VDC_F) > accuracy) || ((VDC_F-pre_vdc_f) > accuracy))
	{
		if (!Ivi_Simulating(vi))
		{
			ViSession   io = Ivi_IOSession(vi);
			viCheckErr( viPrintf (io, "CONF:OUTP?\n"));
			Delay (delay_time);
			viCheckErr( viScanf (io, "%s", output)); 
			Delay (delay_time);
		}
		else
		{
			checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_OUTPUT, 0,
													&get_status));
			if (get_status == VI_TRUE)
				Fmt (output, ON_STRING);
			else if (get_status == VI_FALSE)
				Fmt (output, OFF_STRING);
		}
			
		if(stricmp(output,ON_STRING) == 0)
			checkErr( error=CHR62000_ERROR_OUTPUT_IS_ON ); 
		
		viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_ON_RISE, IVI_VAL_DIRECT_USER_CALL,
												VDC_R), 2, "VDC_R");
		viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_ON_FALL, IVI_VAL_DIRECT_USER_CALL,
												VDC_F), 3, "VDC_F");
	}												
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetCurrent                                       
 * Purpose:  This function set the current to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetCurrent (ViSession vi,
                                       ViReal64 current)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	curr_limit_high=0,curr_limit_low=0,volt_main=0,MaxPower=0;
	ViInt32		apg_status=-1;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_APG_MODE, 0, &apg_status));	
	if ( apg_status==2 || apg_status==3)
		checkErr( error=CHR62000_ERROR_APG_CONTROLLING );  
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_LIMIT_HIGH, IVI_VAL_DIRECT_USER_CALL,
											&curr_limit_high));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_LIMIT_LOW, IVI_VAL_DIRECT_USER_CALL,
											&curr_limit_low));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLTAGE, IVI_VAL_DIRECT_USER_CALL,
											&volt_main));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_MAXPOWER, 0, &MaxPower)); 											
		
	// if current > curr_limit_high or current < curr_limit_low, then error
	if ( ((current-curr_limit_high) > accuracy) || ((curr_limit_low-current) > accuracy) )
		checkErr( error= CHR62000_ERROR_OUT_OF_CURR_LIMIT );											
	/*else if ( (volt_main*current-MaxPower) > accuracy) 	// if volt_main*current setting bigger than MaxPower, then error 	
		checkErr( error= CHR62000_ERROR_OUT_OF_MAXPOWER );*/
		else	// the current setting is in current limit, and in MaxPower range, then write current setting to instrument
			viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURRENT, IVI_VAL_DIRECT_USER_CALL,
													current), 2, "Current");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetCurrentLimit                                       
 * Purpose:  This function set the current high/low limit to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetCurrentLimit (ViSession vi,
                                            ViBoolean highLow,
                                            ViReal64 currentLimit)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	curr_main=0,curr_limit_high=0,curr_limit_low=0;  	
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( highLow!=VI_TRUE && highLow!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURRENT, IVI_VAL_DIRECT_USER_CALL,
											&curr_main));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_LIMIT_HIGH, IVI_VAL_DIRECT_USER_CALL,
											&curr_limit_high));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_LIMIT_LOW, IVI_VAL_DIRECT_USER_CALL,
											&curr_limit_low));
	
	if (highLow == VI_TRUE)
	{
		if ( (curr_limit_low-currentLimit) > accuracy )
			checkErr( error= CHR62000_ERROR_INVALID_PARAMETER );
		else
		{
			viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_LIMIT_HIGH, IVI_VAL_DIRECT_USER_CALL,
													currentLimit), 3, "Current Limit");
			if ( (curr_main-currentLimit) > accuracy )	
				checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_CURRENT));
		}													
	}												
	
	else if (highLow == VI_FALSE)
	{
		if ( (currentLimit-curr_limit_high) > accuracy )
			checkErr( error= CHR62000_ERROR_INVALID_PARAMETER );
		else
		{
			viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_LIMIT_LOW, IVI_VAL_DIRECT_USER_CALL,
													currentLimit), 3, "Current Limit");
			if ( (currentLimit-curr_main) > accuracy )	
				checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_CURRENT));													
		}														
	}											
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetCurrentProtect                                       
 * Purpose:  This function set the current protect to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetCurrentProtect (ViSession vi,
                                              ViReal64 currentProtect)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	curr_main=0,min_curr_protect=0;
	ViInt32		value=0;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURRENT, IVI_VAL_DIRECT_USER_CALL,
											&curr_main));
	value=(ViInt32)(curr_main*OCP_RATIO*1000); //the protect current can not less than OCP_RATIO*current setting
	if ( (value%100) !=0)
    {
    	value=value+100-(value%100);
    	min_curr_protect=(((ViReal64)(value))/1000);
    }
    else
    	min_curr_protect=(((ViReal64)(value))/1000);
	
	if ( (min_curr_protect-currentProtect) > accuracy)
		checkErr( error= CHR62000_ERROR_OUT_OF_OCP );
	else													
		viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_PROTECT, IVI_VAL_DIRECT_USER_CALL,
												currentProtect), 2, "Current Protect");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetCurrentSlew                                       
 * Purpose:  This function set the current slew rate to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetCurrentSlew (ViSession vi,
                                           ViReal64 currentSlew)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	checkErr( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_CURR_SLEW_INF, 0, VI_FALSE));
	viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_SLEW, IVI_VAL_DIRECT_USER_CALL,
											currentSlew), 2, "Current Slew");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetCurrentSlewINF                                       
 * Purpose:  This function set the current slew rate infinite to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetCurrentSlewINF (ViSession vi,
                                              ViBoolean currentSlewInfinite)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( currentSlewInfinite!=VI_TRUE && currentSlewInfinite!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	viCheckParm( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_CURR_SLEW_INF, 0,
											currentSlewInfinite), 2, "Current Slew Infinite");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetPowerProtect                                       
 * Purpose:  This function set the power protect to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetPowerProtect (ViSession vi,
                                            ViReal64 powerProtect)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_POWER_PROTECT, IVI_VAL_DIRECT_USER_CALL,
											powerProtect), 2, "Power Protect");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetBacklight   
 * Purpose:  This function set the backlight to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetBacklight (ViSession vi,
                                         ViInt32 backlight)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_BACKLIGHT, 0,
											backlight), 2, "Backlight");
											
	checkErr( chr62000_CheckStatus (vi));    
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetOutput                                       
 * Purpose:  This function enable the output to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetOutput (ViSession vi,
                                      ViBoolean output)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		Inh_Status=0;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( output!=VI_TRUE && output!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_INHIBIT, 0,
											&Inh_Status));
	if ( Inh_Status!=0 )
		checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_OUTPUT)); 
	viCheckParm( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_OUTPUT, 0,
											output), 2, "Output");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetTTLPort                                       
 * Purpose:  This function set the TTL value to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetTTLPort (ViSession vi,
                                       ViInt32 TTLValue)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_TTL_PORT, IVI_VAL_DIRECT_USER_CALL,
											TTLValue), 2, "TTL Value");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetBeeper                                       
 * Purpose:  This function set the beeper state to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetBeeper (ViSession vi,
                                      ViBoolean beeper)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( beeper!=VI_TRUE && beeper!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
		
	viCheckParm( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_BEEPER, 0,
											beeper), 2, "Beeper");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetRmtMode                                       
 * Purpose:  This function set the remote state to the instrument (RS232C only)
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetRmtMode (ViSession vi,
                                       ViBoolean remoteMode)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( remoteMode!=VI_TRUE && remoteMode!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
		
	viCheckParm( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_REMOTE_MODE, 0,
													remoteMode), 2, "Remote Mode");
	
	if ( remoteMode == VI_FALSE)
	{
		checkErr( Ivi_InvalidateAllAttributes (vi)); 														
		//checkErr( chr62000_QueryMaxMin (vi));
	}	
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetFoldbackProtect                                       
 * Purpose:  This function set the foldback state and foldback delay time 
 *           to the instrument 
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetFoldbackProtect (ViSession vi,
                                               ViInt32 foldbackProtect,
                                               ViReal64 foldbackDelayTime)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));

	checkErr( chr62000_MST_APG_PRG_status (vi, 3));
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_FOLDBACK_PROTECT, 0,
											foldbackProtect), 2, "Foldback Protect");
	viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_FOLDBACK_DELAY_TIME, IVI_VAL_DIRECT_USER_CALL,
											foldbackDelayTime), 3, "Foldback Delay Time");											
												
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetAPGMode   
 * Purpose:  This function set the APG state and APG reference voltage
 *           to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetAPGMode (ViSession vi,
                                       ViInt32 APGMode,
                                       ViBoolean APGReferenceVoltage)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));

	if ( APGReferenceVoltage!=VI_TRUE && APGReferenceVoltage!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );	
	
	checkErr( chr62000_MST_APG_PRG_status (vi, 3));				
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_APG_MODE, 0,
											APGMode), 2, "APG Mode");
	viCheckParm( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_APG_REFERENCE_VOLTAGE, 0,
											APGReferenceVoltage), 3, "APG Reference Voltage");											
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetMeasSpeed   
 * Purpose:  This function set the reading speed of AD to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetMeasSpeed (ViSession vi,
                                         ViInt32 measSpeed)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_MEASSPD, 0,
											measSpeed), 2, "Meas Speed");
											
	checkErr( chr62000_CheckStatus (vi));    
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetAVGTimes   
 * Purpose:  This function set the average times of AD to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetAVGTimes (ViSession vi,
                                        ViInt32 averageTimes)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_AVG_TIMES, 0,
											averageTimes), 2, "Average Times");
											
	checkErr( chr62000_CheckStatus (vi));    
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetAVGMethod                                       
 * Purpose:  This function set the average method of AD to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetAVGMethod (ViSession vi,
                                         ViBoolean averageMethod)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( averageMethod!=VI_TRUE && averageMethod!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
		
	viCheckParm( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_AVG_METHOD, 0,
											averageMethod), 2, "Average Method");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetRmtInhibit   
 * Purpose:  This function set the remote inhibit to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetRmtInhibit (ViSession vi,
                                          ViInt32 remoteInhibit)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_INHIBIT, 0,
											remoteInhibit), 2, "Remote Inhibit");
											
	checkErr( chr62000_CheckStatus (vi));    
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_ProgSel   
 * Purpose:  This function set the program No., link and count to the
 *           instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_ProgSel (ViSession vi,
                                    ViInt32 programSelected, ViInt32 link,
                                    ViInt32 count, ViBoolean programClear)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		channel[10];
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( programClear!=VI_TRUE && programClear!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
		
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_PROG_SELECTED, IVI_VAL_DIRECT_USER_CALL,
										programSelected), 2, "Program Selected");
										
	if (programClear == VI_TRUE)											
	{
		if (!Ivi_Simulating(vi))
		{
			ViSession   io = Ivi_IOSession(vi);
			viCheckErr( viPrintf (io, "PROG:CLEAR\n")); 
			Delay (0.05);
		}	
		checkErr( chr62000_Invalidate_Program (vi, programSelected));
	}
	else
	{
		Fmt (channel, "%d", programSelected);
		viCheckParm( Ivi_SetAttributeViInt32 (vi, channel, CHR62000_ATTR_PROG_LINK, 0,
												link), 3, "Link");
		viCheckParm( Ivi_SetAttributeViInt32 (vi, channel, CHR62000_ATTR_PROG_COUNT, 0,
												count), 4, "Count");
	}
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_ProgAdd   
 * Purpose:  This function adds sequences to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_ProgAdd (ViSession vi,
                                    ViInt32 addSequence)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	
/*	if ( addSequence<1  || addSequence>100 )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
		
	if (!Ivi_Simulating(vi))
    {
		ViSession   io = Ivi_IOSession(vi);
		checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));
		viCheckErr( viPrintf (io, "PROG:ADD %d\n", addSequence));
		Delay (delay_time);
	}							*/
	
	checkErr( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_PROG_ADD,IVI_VAL_DIRECT_USER_CALL, addSequence));					
	
	//modify by justin.hsu 12/03/08 						
	
	
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetSeqPara   
 * Purpose:  This function set the sequence parameter to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetSeqPara (ViSession vi,
                                       ViInt32 sequenceSelected, ViInt32 type,
                                       ViReal64 voltage, ViReal64 voltageSlew,
                                       ViReal64 current, ViReal64 currentSlew,
                                       ViInt32 TTL, ViReal64 time)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		channel[10]; 
	ViInt32		programSelected=0,ex_type=8;
	ViReal64	volt_limit_high=0,volt_limit_low=0,curr_limit_high=0,curr_limit_low=0; 
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( type<0  || type>3 )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
		
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_LIMIT_HIGH, IVI_VAL_DIRECT_USER_CALL,
											&volt_limit_high));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_LIMIT_LOW, IVI_VAL_DIRECT_USER_CALL,
											&volt_limit_low));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_LIMIT_HIGH, IVI_VAL_DIRECT_USER_CALL,
											&curr_limit_high));
	checkErr( Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_LIMIT_LOW, IVI_VAL_DIRECT_USER_CALL,
											&curr_limit_low));											
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_PROG_SELECTED,
										IVI_VAL_DIRECT_USER_CALL, &programSelected));
	checkErr( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_PROG_SEQ_SELECTED,
										IVI_VAL_DIRECT_USER_CALL, sequenceSelected));					//modify by justin.hsu 12/03/08 (1-10 -> 100)						
	Fmt (channel,"%d", sequenceSelected); //	calculate channel for specific program and sequence		//modify by justin.hsu 12/03/08 
	
	// if voltage > volt_limit_high or voltage < volt_limit_low, then error
	if ( ((voltage-volt_limit_high) > accuracy) || ((volt_limit_low-voltage) > accuracy) )
		checkErr( error= CHR62000_ERROR_OUT_OF_VOLT_LIMIT );
	else													
		viCheckParm( Ivi_SetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_VOLTAGE, 0,
												voltage), 4, "Voltage");											
	// if current > curr_limit_high or current < curr_limit_low, then error
	if ( ((current-curr_limit_high) > accuracy) || ((curr_limit_low-current) > accuracy) )
		checkErr( error= CHR62000_ERROR_OUT_OF_CURR_LIMIT );											
	else													
		viCheckParm( Ivi_SetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_CURRENT, 0,
												current), 6, "Current");	
												
	viCheckParm( Ivi_SetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_VOLT_SLEW, 0,
											voltageSlew), 5, "Voltage Slew");											
	viCheckParm( Ivi_SetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_CURR_SLEW, 0,
											currentSlew), 7, "Current Slew");
	viCheckParm( Ivi_SetAttributeViInt32 (vi, channel, CHR62000_ATTR_PROG_SEQ_TTL, 0,
											TTL), 8, "TTL");
	if ( type == 0 )  // only when type=AUTO, time will write into the attribute
		viCheckParm( Ivi_SetAttributeViReal64 (vi, channel, CHR62000_ATTR_PROG_SEQ_TIME, 0,
												time), 9, "Time");		
	// get ex_type to compare, if ex_type!=type, then invalidate CHR62000_ATTR_PROG_SEQ_TIME
	checkErr( Ivi_GetAttributeViInt32 (vi, channel, CHR62000_ATTR_PROG_SEQ_TYPE, 
                                           0, &ex_type));												
	viCheckParm( Ivi_SetAttributeViInt32 (vi, channel, CHR62000_ATTR_PROG_SEQ_TYPE, 0,
											type), 3, "Type");
	if ( type != ex_type)										
		checkErr( Ivi_InvalidateAttribute (vi, channel, CHR62000_ATTR_PROG_SEQ_TIME));											

	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_ProgRun   
 * Purpose:  This function set the program running state to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_ProgRun (ViSession vi, ViBoolean run)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( run!=VI_TRUE && run!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
		
	checkErr( chr62000_MST_APG_PRG_status (vi, 0));
	viCheckParm( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_PROG_RUN, 0,
											run), 2, "Run");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SaveProgram   
 * Purpose:  This function save the program parameter to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SaveProgram (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viWrite (io, "PROG:SAVE\n", 10, VI_NULL));		
		Delay (1); 
	}
	
	checkErr( chr62000_CheckStatus (vi));

Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_ProgMode   
 * Purpose:  This function set the program mode to the instrument's output
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_ProgMode (ViSession vi,
                                     ViInt32 programMode)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_PROG_MODE, 0,
											programMode), 2, "Program Mode");
											
	checkErr( chr62000_CheckStatus (vi));    
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetStepPara
 * Purpose:  This function set the program step mode start voltage, 
 *           end voltage and time to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetStepPara (ViSession vi,
                                        ViReal64 startVoltage,
                                        ViReal64 endVoltage, ViReal64 time)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL));

	viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PROG_STEP_STARTV, 0,
											startVoltage), 2, "Start Voltage");	
	viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PROG_STEP_ENDV, 0,
											endVoltage), 3, "End Voltage");												
	viCheckParm( Ivi_SetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PROG_STEP_TIME, 0,
											time), 4, "Time");											
											
	checkErr( chr62000_CheckStatus (vi));    
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetMstSlvID   
 * Purpose:  This function set Master or Slave to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetMstSlvID (ViSession vi, ViInt32 ID)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		PRE_MSTSLV_ID=-1; 
 
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_MSTSLV_ID, 0,
								&PRE_MSTSLV_ID));
	if ( PRE_MSTSLV_ID!=ID )
	{
		checkErr( chr62000_MST_APG_PRG_status (vi, 0));
		checkErr( chr62000_MST_APG_PRG_status (vi, 1)); 
		checkErr( chr62000_MST_APG_PRG_status (vi, 2));
		viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_MSTSLV_ID, 0,
												ID), 2, "ID");
		if ( PRE_MSTSLV_ID==0 )
			chr62000_Specify_Cache (vi, 1, VI_FALSE); 	                    	           		     
		if ( ID==0 )
			chr62000_Specify_Cache (vi, 1, VI_TRUE);
			
		chr62000_Specify_Cache (vi, 2, VI_FALSE); //restore single machine rating 
	}	
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetMasterPara   
 * Purpose:  This function set the parallel/series state and slave numbers 
 *			 to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetMasterPara (ViSession vi,
                                          ViBoolean parallelSeries,
                                          ViInt32 slaveNumbers)
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean	pre_parallelSeries = VI_TRUE; 
	ViInt32		pre_slaveNumbers=0;
	
	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	
	if ( parallelSeries!=VI_TRUE && parallelSeries!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );	
	
	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_PARALLEL_SERIES, 0,
								&pre_parallelSeries));
	checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_SLAVE_NUMBERS, 0,
								&pre_slaveNumbers));
	if ( (pre_parallelSeries!=parallelSeries) || (pre_slaveNumbers!=slaveNumbers) )
	{
		checkErr( chr62000_MST_APG_PRG_status (vi, 0));		
		checkErr( chr62000_MST_APG_PRG_status (vi, 1));	
		checkErr( chr62000_MST_APG_PRG_status (vi, 2));	
		viCheckParm( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_PARALLEL_SERIES, 0,
												parallelSeries), 2, "Parallel Series");		
		viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_SLAVE_NUMBERS, 0,
												slaveNumbers), 3, "Slave Numbers");
	}											
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_MstEnable   
 * Purpose:  This function set the master control enable state to the instrument
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_MstEnable (ViSession vi,
                                      ViBoolean masterEnable)
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean	pre_masterEnable = VI_TRUE;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	
	if ( masterEnable!=VI_TRUE && masterEnable!=VI_FALSE )
		checkErr( error=CHR62000_ERROR_INVALID_PARAMETER );
		
	checkErr( Ivi_GetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_MASTER_ENABLE, 0,
								&pre_masterEnable));
			
	if ( pre_masterEnable!=masterEnable )
	{
		checkErr( chr62000_MST_APG_PRG_status (vi, 1));	
		checkErr( chr62000_MST_APG_PRG_status (vi, 2));	
		checkErr( chr62000_MST_APG_PRG_status (vi, 4));
		viCheckParm( Ivi_SetAttributeViBoolean (vi, VI_NULL, CHR62000_ATTR_MASTER_ENABLE, 0,
												masterEnable), 2, "Master Enable");
		Delay (master_enable_delay_time);												
		if ( masterEnable==VI_TRUE)
			chr62000_Specify_Cache (vi, 2, VI_TRUE);  //increase rating
		else
			chr62000_Specify_Cache (vi, 2, VI_FALSE); //restore single machine rating
	}												
	checkErr( chr62000_CheckStatus (vi)); 
			
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetCLS   
 * Purpose:  This function clear the instrument status
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetCLS (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viWrite (io, "*CLS\n", 5, VI_NULL));		
		Delay (delay_time); 
	}
	checkErr( chr62000_CheckStatus (vi));

Error:
	Ivi_UnlockSession (vi, VI_NULL); 
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetESE   
 * Purpose:  This function set the standard event status enable register to 
 *           the instrument 
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetESE (ViSession vi,
                                   ViInt32 enableRegister)		 
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_STATUS_ESE, IVI_VAL_DIRECT_USER_CALL,
											enableRegister), 2, "Enable Register");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetSRE   
 * Purpose:  This function set service request enable register to the 
 *           instrument 
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetSRE (ViSession vi,
                                   ViInt32 enableRegister)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));
	viCheckParm( Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_STATUS_SRE, IVI_VAL_DIRECT_USER_CALL,
											enableRegister), 2, "Enable Register");
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_Save   
 * Purpose:  This function store the whole parameter to the instrument memory
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Save (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;

	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE)); 
		viCheckErr( viWrite (io, "*SAV\n", 5, VI_NULL));		
		Delay (1); 
	}
	checkErr( chr62000_CheckStatus (vi)); 

Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_Recall   
 * Purpose:  This function recall the whole parameter from the instrument 
 *           memory
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Recall (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		checkErr( chr62000_MST_APG_PRG_status (vi, 3));
		checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));
		viCheckErr( viWrite (io, "*RCL 1\n", 7, VI_NULL));		
		Delay (1); 
		checkErr( Ivi_InvalidateAllAttributes (vi));
		checkErr( chr62000_QueryMaxMin (vi));
	}
	checkErr( chr62000_CheckStatus (vi));  

Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetOPC   
 * Purpose:  This function set the OPC bit (bit 0) of the Standard Event 
 *           Status register when the instrument has completed all 
 *           pending operations
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetOPC (ViSession vi) 
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL)); 
	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viWrite (io, "*OPC\n", 5, VI_NULL));		
		Delay (delay_time); 
	}
	checkErr( chr62000_CheckStatus (vi));

Error:
	Ivi_UnlockSession (vi, VI_NULL); 
	return error;
}


/*****************************************************************************
 * Function: chr62000_Abort                                       
 * Purpose:  This function set all output state to "OFF"
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_Abort (ViSession vi) 
{
	ViStatus	error = VI_SUCCESS;
	
	checkErr( Ivi_LockSession (vi, VI_NULL));

	if (!Ivi_Simulating(vi))
	{
		ViSession   io = Ivi_IOSession(vi);
		viCheckErr( viWrite (io, "ABOR\n", 5, VI_NULL));		
		Delay (delay_time); 
	}
	
	checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_OUTPUT));
	checkErr( chr62000_CheckStatus (vi)); 
	
Error:
	Ivi_UnlockSession (vi, VI_NULL);
	return error;
}

/*****************************************************************************
 * Function: chr62000_SetAttribute<type> Functions                                    
 * Purpose:  These functions enable the instrument driver user to set 
 *           attribute values directly.  There are typesafe versions for 
 *           ViInt32, ViReal64, ViString, ViBoolean, and ViSession datatypes.                                         
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_SetAttributeViInt32 (ViSession vi, ViConstString channelName, 
                                                ViAttr attributeId, ViInt32 value)
{                                                                                                           
    return Ivi_SetAttributeViInt32 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                    value);
}                                                                                                           
ViStatus _VI_FUNC chr62000_SetAttributeViReal64 (ViSession vi, ViConstString channelName, 
                                                 ViAttr attributeId, ViReal64 value)
{                                                                                                           
    return Ivi_SetAttributeViReal64 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                     value);
}                                                                                                           
ViStatus _VI_FUNC chr62000_SetAttributeViString (ViSession vi, ViConstString channelName, 
                                                 ViAttr attributeId, ViConstString value) 
{   
    return Ivi_SetAttributeViString (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                     value);
}   
ViStatus _VI_FUNC chr62000_SetAttributeViBoolean (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViBoolean value)
{                                                                                                           
    return Ivi_SetAttributeViBoolean (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}                                                                                                           
ViStatus _VI_FUNC chr62000_SetAttributeViSession (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViSession value)
{                                                                                                           
    return Ivi_SetAttributeViSession (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}                                                                                                           

/*****************************************************************************
 * Function: chr62000_CheckAttribute<type> Functions                                  
 * Purpose:  These functions enable the instrument driver user to check  
 *           attribute values directly.  These functions check the value you
 *           specify even if you set the CHR62000_ATTR_RANGE_CHECK 
 *           attribute to VI_FALSE.  There are typesafe versions for ViInt32, 
 *           ViReal64, ViString, ViBoolean, and ViSession datatypes.                         
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_CheckAttributeViInt32 (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViInt32 value)
{                                                                                                           
    return Ivi_CheckAttributeViInt32 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}
ViStatus _VI_FUNC chr62000_CheckAttributeViReal64 (ViSession vi, ViConstString channelName, 
                                                   ViAttr attributeId, ViReal64 value)
{                                                                                                           
    return Ivi_CheckAttributeViReal64 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                       value);
}                                                                                                           
ViStatus _VI_FUNC chr62000_CheckAttributeViString (ViSession vi, ViConstString channelName, 
                                                   ViAttr attributeId, ViConstString value)  
{   
    return Ivi_CheckAttributeViString (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                       value);
}   
ViStatus _VI_FUNC chr62000_CheckAttributeViBoolean (ViSession vi, ViConstString channelName, 
                                                    ViAttr attributeId, ViBoolean value)
{                                                                                                           
    return Ivi_CheckAttributeViBoolean (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                        value);
}                                                                                                           
ViStatus _VI_FUNC chr62000_CheckAttributeViSession (ViSession vi, ViConstString channelName, 
                                                    ViAttr attributeId, ViSession value)
{                                                                                                           
    return Ivi_CheckAttributeViSession (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                        value);
}                                                                                                           

/*****************************************************************************
 * Function: chr62000_GetNextCoercionRecord                        
 * Purpose:  This function enables the instrument driver user to obtain
 *           the coercion information associated with the IVI session.                                                              
 *           This function retrieves and clears the oldest instance in which 
 *           the instrument driver coerced a value the instrument driver user
 *           specified to another value.                     
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_GetNextCoercionRecord (ViSession vi,
                                                  ViInt32 bufferSize,
                                                  ViChar  record[])
{
    return Ivi_GetNextCoercionString (vi, bufferSize, record);
}

/*****************************************************************************
 * Function: chr62000_LockSession and chr62000_UnlockSession Functions                        
 * Purpose:  These functions enable the instrument driver user to lock the 
 *           session around a sequence of driver calls during which other
 *           execution threads must not disturb the instrument state.
 *                                                                          
 *           NOTE:  The callerHasLock parameter must be a local variable 
 *           initialized to VI_FALSE and passed by reference, or you can pass 
 *           VI_NULL.                     
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_LockSession (ViSession vi, ViBoolean *callerHasLock)  
{                                              
    return Ivi_LockSession(vi,callerHasLock);      
}                                              
ViStatus _VI_FUNC chr62000_UnlockSession (ViSession vi, ViBoolean *callerHasLock) 
{                                              
    return Ivi_UnlockSession(vi,callerHasLock);    
}   

/*****************************************************************************
 * Function: chr62000_GetErrorInfo and chr62000_ClearErrorInfo Functions                       
 * Purpose:  These functions enable the instrument driver user to  
 *           get or clear the error information the driver associates with the
 *           IVI session.                                                        
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_GetErrorInfo (ViSession vi, ViStatus *primaryError, 
                                         ViStatus *secondaryError, ViChar errorElaboration[256])  
{                                                                                                           
    return Ivi_GetErrorInfo(vi, primaryError, secondaryError, errorElaboration);                                
}                                                                                                           
ViStatus _VI_FUNC chr62000_ClearErrorInfo (ViSession vi)                                                        
{                                                                                                           
    return Ivi_ClearErrorInfo (vi);                                                                             
}

/*****************************************************************************
 * Function: WriteInstrData and ReadInstrData Functions                      
 * Purpose:  These functions enable the instrument driver user to  
 *           write and read commands directly to and from the instrument.            
 *                                                                           
 *           Note:  These functions bypass the IVI attribute state caching.  
 *                  WriteInstrData invalidates the cached values for all 
 *                  attributes.
 *****************************************************************************/
ViStatus _VI_FUNC chr62000_WriteInstrData (ViSession vi, ViConstString writeBuffer)   
{   
    return Ivi_WriteInstrData (vi, writeBuffer);    
}   
ViStatus _VI_FUNC chr62000_ReadInstrData (ViSession vi, ViInt32 numBytes, 
                                          ViChar rdBuf[], ViInt32 *bytesRead)  
{   
    return Ivi_ReadInstrData (vi, numBytes, rdBuf, bytesRead);   
} 

/*****************************************************************************
 *-------------------- Utility Functions (Not Exported) ---------------------*
 *****************************************************************************/

/*****************************************************************************
 * Function: chr62000_CheckStatus                                                 
 * Purpose:  This function checks the status of the instrument to detect 
 *           whether the instrument has encountered an error.  This function  
 *           is called at the end of most exported driver functions.  If the    
 *           instrument reports an error, this function returns      
 *           IVI_ERROR_INSTR_SPECIFIC.  The user can set the 
 *           IVI_ATTR_QUERY_INSTR_STATUS attribute to VI_FALSE to disable this 
 *           check and increase execution speed.                                   
 *
 *           Note:  Call this function only when the session is locked.
 *****************************************************************************/
static ViStatus chr62000_CheckStatus (ViSession vi)		  
{
    ViStatus    error = VI_SUCCESS;

    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
        {
        checkErr( chr62000_CheckStatusCallback (vi, Ivi_IOSession(vi)));
        checkErr( Ivi_SetNeedToCheckStatus (vi, VI_FALSE));
        }
        
Error:
    return error;
}

/*****************************************************************************
 * Function: chr62000_WaitForOPC                                                  
 * Purpose:  This function waits for the instrument to complete the      
 *           execution of all pending operations.  This function is a        
 *           wrapper for the WaitForOPCCallback.  It can be called from 
 *           other instrument driver functions. 
 *
 *           The maxTime parameter specifies the maximum time to wait for
 *           operation complete in milliseconds.
 *
 *           Note:  Call this function only when the session is locked.
 *****************************************************************************/
static ViStatus chr62000_WaitForOPC (ViSession vi, ViInt32 maxTime) 
{
    ViStatus    error = VI_SUCCESS;

    if (!Ivi_Simulating(vi))
        {
        ViInt32  oldOPCTimeout; 
        
        checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_OPC_TIMEOUT, 
                                           0, &oldOPCTimeout));
        Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_OPC_TIMEOUT,        
                                 0, maxTime);

        error = chr62000_WaitForOPCCallback (vi, Ivi_IOSession(vi));

        Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_OPC_TIMEOUT, 
                                 0, oldOPCTimeout);
        viCheckErr( error);
        }
Error:
    return error;
}

/*****************************************************************************
 * Function: chr62000_DefaultInstrSetup                                               
 * Purpose:  This function sends a default setup to the instrument.  The    
 *           chr62000_reset function calls this function.  The 
 *           chr62000_IviInit function calls this function when the
 *           user passes VI_FALSE for the reset parameter.  This function is 
 *           useful for configuring settings that other instrument driver 
 *           functions require.    
 *
 *           Note:  Call this function only when the session is locked.
 *****************************************************************************/
static ViStatus chr62000_DefaultInstrSetup (ViSession vi)
{
    ViStatus    error = VI_SUCCESS;
        
    /* Invalidate all attributes */
    checkErr( Ivi_InvalidateAllAttributes (vi));
    checkErr( chr62000_QueryMaxMin (vi));
    
    if (!Ivi_Simulating(vi))
        {
        ViSession   io = Ivi_IOSession(vi); /* call only when locked */

        checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));
		
		viCheckErr( viPrintf (io, "*CLS\n"));
		
		// viCheckErr( viPrintf (io, ":HEADERS OFF"));
        }
Error:
    return error;
}

/*****************************************************************************
 * Function: ReadToFile and WriteFromFile Functions                          
 * Purpose:  Functions for instrument driver developers to read/write        
 *           instrument data to/from a file.                                 
 *****************************************************************************/
static ViStatus chr62000_ReadToFile (ViSession vi, ViConstString filename, 
                                     ViInt32 maxBytesToRead, ViInt32 fileAction, 
                                     ViInt32 *totalBytesWritten)  
{   
    return Ivi_ReadToFile (vi, filename, maxBytesToRead, fileAction, totalBytesWritten);  
}   
static ViStatus chr62000_WriteFromFile (ViSession vi, ViConstString filename, 
                                        ViInt32 maxBytesToWrite, ViInt32 byteOffset, 
                                        ViInt32 *totalBytesWritten) 
{   
    return Ivi_WriteFromFile (vi, filename, maxBytesToWrite, byteOffset, totalBytesWritten); 
}

/*****************************************************************************
 *------------------------ Global Session Callbacks -------------------------*
 *****************************************************************************/

/*****************************************************************************
 * Function: chr62000_CheckStatusCallback                                               
 * Purpose:  This function queries the instrument to determine if it has 
 *           encountered an error.  If the instrument has encountered an 
 *           error, this function returns the IVI_ERROR_INSTRUMENT_SPECIFIC 
 *           error code.  This function is called by the 
 *           chr62000_CheckStatus utility function.  The 
 *           Ivi_SetAttribute and Ivi_GetAttribute functions invoke this 
 *           function when the optionFlags parameter includes the
 *           IVI_VAL_DIRECT_USER_CALL flag.
 *           
 *           The user can disable calls to this function by setting the 
 *           IVI_ATTR_QUERY_INSTR_STATUS attribute to VI_FALSE.  The driver can 
 *           disable the check status callback for a particular attribute by 
 *           setting the IVI_VAL_DONT_CHECK_STATUS flag.
 *****************************************************************************/
static ViStatus _VI_FUNC chr62000_CheckStatusCallback (ViSession vi, ViSession io)
{
    ViStatus    error = VI_SUCCESS;
    
    
    /* Query instrument status */

        ViInt16     esr = 0; 
        
        //viCheckErr( viQueryf (io, "*ESR?\n", "%hd", &esr)); 
        
        viCheckErr( viPrintf (io, "*ESR?\n"));
		viCheckErr( viScanf (io, "%hd", &esr)); 
        if (esr & IEEE_488_2_ERROR_BITS)
                {     
                viCheckErr( IVI_ERROR_INSTR_SPECIFIC);
                }
            
Error:
    return error;
}
/*****************************************************************************
 * Function: chr62000_WaitForOPCCallback                                               
 * Purpose:  This function waits until the instrument has finished processing 
 *           all pending operations.  This function is called by the 
 *           chr62000_WaitForOPC utility function.  The IVI engine invokes
 *           this function in the following two cases:
 *           - Before invoking the read callback for attributes for which the 
 *             IVI_VAL_WAIT_FOR_OPC_BEFORE_READS flag is set.
 *           - After invoking the write callback for attributes for which the 
 *             IVI_VAL_WAIT_FOR_OPC_AFTER_WRITES flag is set.
 *****************************************************************************/
static ViStatus _VI_FUNC chr62000_WaitForOPCCallback (ViSession vi, ViSession io)
{
    ViStatus    error = VI_SUCCESS;

    return error;
}

/*****************************************************************************
 *----------------- Attribute Range Tables and Callbacks --------------------*
 *****************************************************************************/

/*- CHR62000_ATTR_ID_QUERY_RESPONSE -*/

static ViStatus _VI_FUNC chr62000AttrIdQueryResponse_ReadCallback (ViSession vi, 
                                                                   ViSession io,
                                                                   ViConstString channelName, 
                                                                   ViAttr attributeId,
                                                                   const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      rdBuffer[BUFFER_SIZE];
    ViUInt32    retCnt;
    
    viCheckErr( viPrintf (io, "*IDN?\n"));
    viCheckErr( viRead (io, rdBuffer, BUFFER_SIZE-1, &retCnt));
    rdBuffer[retCnt] = 0;

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, rdBuffer));
    
Error:
    return error;
}		 
    
/*- CHR62000_ATTR_SPECIFIC_DRIVER_REVISION -*/

static ViStatus _VI_FUNC chr62000AttrDriverRevision_ReadCallback (ViSession vi, 
                                                                  ViSession io,
                                                                  ViConstString channelName, 
                                                                  ViAttr attributeId,
                                                                  const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      driverRevision[256];
    
    
    sprintf (driverRevision, 
             "Driver: chr62000 %.2f, Compiler: %s %3.2f, "
             "Components: IVIEngine %.2f, VISA-Spec %.2f",
             CHR62000_MAJOR_VERSION + CHR62000_MINOR_VERSION/1000.0, 
             IVI_COMPILER_NAME, IVI_COMPILER_VER_NUM, 
             IVI_ENGINE_MAJOR_VERSION + IVI_ENGINE_MINOR_VERSION/1000.0, 
             Ivi_ConvertVISAVer(VI_SPEC_VERSION));

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, driverRevision));    
Error:
    return error;
}				  

															  
/*- CHR62000_ATTR_INSTRUMENT_FIRMWARE_REVISION -*/

static ViStatus _VI_FUNC chr62000AttrFirmwareRevision_ReadCallback (ViSession vi, 
                                                                    ViSession io,
                                                                    ViConstString channelName, 
                                                                    ViAttr attributeId,
                                                                    const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      instrRev[BUFFER_SIZE];
    ViChar      tmp[BUFFER_SIZE],tmp1[BUFFER_SIZE];
    ViChar		*dot;
    ViInt32		n=0;
    
    viCheckErr( viPrintf (io, "*IDN?\n"));

	viCheckErr( viScanf (io, "%*[^,],%*[^,],%256[^,],%256[^,]", tmp, tmp1));
	dot=strchr (tmp, 46); // 46='.', search decimal point
	if ( dot==NULL )
		sprintf (tmp,"%s",tmp1);
	n=strlen(tmp);
	if (tmp[n-1]==10)
	tmp[n-1] = 0;
	sprintf (instrRev,"%s",tmp);

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, instrRev));
    
Error:
    return error;
}

/*- CHR62000_ATTR_INSTRUMENT_MANUFACTURER -*/

static ViStatus _VI_FUNC chr62000AttrInstrumentManufacturer_ReadCallback (ViSession vi, 
                                                                          ViSession io,
                                                                          ViConstString channelName, 
                                                                          ViAttr attributeId,
                                                                          const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      rdBuffer[BUFFER_SIZE];
    ViChar      idQ[BUFFER_SIZE];
    
    checkErr( Ivi_GetAttributeViString (vi, "", CHR62000_ATTR_ID_QUERY_RESPONSE,
                                        0, BUFFER_SIZE, idQ));
    sscanf (idQ, "%256[^,]", rdBuffer);

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, rdBuffer));
    
Error:
    return error;
}

    

/*- CHR62000_ATTR_INSTRUMENT_MODEL -*/

static ViStatus _VI_FUNC chr62000AttrInstrumentModel_ReadCallback (ViSession vi, 
                                                                   ViSession io,
                                                                   ViConstString channelName, 
                                                                   ViAttr attributeId,
                                                                   const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      rdBuffer[BUFFER_SIZE];
    ViChar      idQ[BUFFER_SIZE];
    
    checkErr( Ivi_GetAttributeViString (vi, "", CHR62000_ATTR_ID_QUERY_RESPONSE,
                                        0, BUFFER_SIZE, idQ));
    sscanf (idQ, "%*[^,],%256[^,]", rdBuffer);

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, rdBuffer));
    
Error:
    return error;
}
    
static ViStatus _VI_FUNC chr62000AttrVoltage_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltage_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 2); 
	viCheckErr( viPrintf (io, "SOUR:VOLT %.2f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltLimitHigh_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:LIM:HIGH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltLimitHigh_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 1); 
	viCheckErr( viPrintf (io, "SOUR:VOLT:LIM:HIGH %.1f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltLimitLow_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:LIM:LOW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltLimitLow_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 1); 
	viCheckErr( viPrintf (io, "SOUR:VOLT:LIM:LOW %.1f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltProtect_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:PROT:HIGH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltProtect_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 1); 
	viCheckErr( viPrintf (io, "SOUR:VOLT:PROT:HIGH %.1f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltSlew_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:SLEW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltSlew_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 3); 
	viCheckErr( viPrintf (io, "SOUR:VOLT:SLEW %.3f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}
                                                             
static ViStatus _VI_FUNC chr62000AttrCurrent_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}
                                                           
static ViStatus _VI_FUNC chr62000AttrCurrent_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 2); 
	viCheckErr( viPrintf (io, "SOUR:CURR %.2f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}
                                                            
static ViStatus _VI_FUNC chr62000AttrCurrLimitHigh_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR:LIM:HIGH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrLimitHigh_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 1); 
	viCheckErr( viPrintf (io, "SOUR:CURR:LIM:HIGH %.1f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrLimitLow_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR:LIM:LOW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}
                                                                
static ViStatus _VI_FUNC chr62000AttrCurrLimitLow_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 1); 
	viCheckErr( viPrintf (io, "SOUR:CURR:LIM:LOW %.1f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}
                                                                 
static ViStatus _VI_FUNC chr62000AttrCurrProtect_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR:PROT:HIGH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}
                                                               
static ViStatus _VI_FUNC chr62000AttrCurrProtect_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 1); 
	viCheckErr( viPrintf (io, "SOUR:CURR:PROT:HIGH %.1f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}
                                                                
static ViStatus _VI_FUNC chr62000AttrCurrSlew_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		rdBuffer[20];
	
	viCheckErr( viPrintf (io, "SOUR:CURR:SLEW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", rdBuffer));
	if(stricmp(rdBuffer,Slew_INF) == 0)  
		*value=atof (ASCII_INF); 
	else
		*value=atof (rdBuffer);
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrSlew_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 3); 
	viCheckErr( viPrintf (io, "SOUR:CURR:SLEW %.3f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrPowerProtect_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:POW:PROT:HIGH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrPowerProtect_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;

	chr62000_Digit_boundary (&value, 1);
	viCheckErr( viPrintf (io, "SOUR:POW:PROT:HIGH %.1f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}


static ViStatus _VI_FUNC chr62000AttrOutput_WriteCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	if ( value == VI_TRUE)
	{
		viCheckErr( viPrintf (io, "CONF:OUTP ON\n"));
		Delay (delay_time);
	}
	else if ( value == VI_FALSE)
	{
		viCheckErr( viPrintf (io, "CONF:OUTP OFF\n"));
		Delay (delay_time);
	}	
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrTtlPort_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:TTL?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrTtlPort_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:TTL %d\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrBeeper_ReadCallback (ViSession vi,
                                                          ViSession io,
                                                          ViConstString channelName,
                                                          ViAttr attributeId,
                                                          ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[8];
	
	viCheckErr( viPrintf (io, "CONF:BEEP?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,ON_STRING) == 0)
		*value=VI_TRUE;
	else if(stricmp(state,OFF_STRING) == 0)
		*value=VI_FALSE;
			
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrBeeper_WriteCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	if ( value == VI_TRUE)
	{
		viCheckErr( viPrintf (io, "CONF:BEEP ON\n"));
		Delay (delay_time);
	}
	else if ( value == VI_FALSE)
	{
		viCheckErr( viPrintf (io, "CONF:BEEP OFF\n"));
		Delay (delay_time);
	}	
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrRemoteMode_WriteCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViUInt16	nInterFace=0; 
	
	viCheckErr( viGetAttribute (io,  VI_ATTR_INTF_TYPE, &nInterFace)); 
	if ( nInterFace == VI_INTF_ASRL)  
	{
		if ( value == VI_TRUE)
		{
			viCheckErr( viPrintf (io, "CONF:REM ON\n"));
			Delay (delay_time);
		}
		else if ( value == VI_FALSE)
		{
			viCheckErr( viPrintf (io, "CONF:REM OFF\n"));
			Delay (delay_time);
		}	
	}
	
	else
		checkErr( error= CHR62000_ERROR_INVALID_INTERFACE );
		
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrFoldbackDelayTime_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:FOLDT?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrFoldbackDelayTime_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 2); 
	viCheckErr( viPrintf (io, "CONF:FOLDT %.2f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrFoldbackProtect_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[10];
	
	viCheckErr( viPrintf (io, "CONF:FOLD?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"DISABLE") == 0)
		*value=0;
	else if(stricmp(state,"CVTOCC") == 0)
		*value=1;
	else if(stricmp(state,"CCTOCV") == 0)
		*value=2;		

Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrFoldbackProtect_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	
	checkErr( Ivi_GetViInt32EntryFromIndex (value, &attrFoldbackProtectRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	viCheckErr( viPrintf (io, "CONF:FOLD %s\n", Cmd));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrApgMode_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[8];
	
	viCheckErr( viPrintf (io, "CONF:APG?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"NONE") == 0)
		*value=0;
	else if(stricmp(state,"VI") == 0)
		*value=3;		
	else if(stricmp(state,"V") == 0)
		*value=1;
	else if(stricmp(state,"I") == 0)
		*value=2;	
		
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrApgMode_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	
	checkErr( Ivi_GetViInt32EntryFromIndex (value, &attrApgModeRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	viCheckErr( viPrintf (io, "CONF:APG %s\n", Cmd));
	Delay (delay_time);
	
	if (value == 1)
	{
		checkErr( chr62000_AttrFlags_Handle (vi, CHR62000_ATTR_VOLTAGE, flag_plus, IVI_VAL_NEVER_CACHE)); 
		checkErr( chr62000_AttrFlags_Handle (vi, CHR62000_ATTR_CURRENT, flag_minus, IVI_VAL_NEVER_CACHE));   
		checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_CURRENT));
	}											
	else if (value == 2)
	{
		checkErr( chr62000_AttrFlags_Handle (vi, CHR62000_ATTR_CURRENT, flag_plus, IVI_VAL_NEVER_CACHE)); 
		checkErr( chr62000_AttrFlags_Handle (vi, CHR62000_ATTR_VOLTAGE, flag_minus, IVI_VAL_NEVER_CACHE));
		checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VOLTAGE));
		
	}
	else if (value == 3)
	{
		checkErr( chr62000_AttrFlags_Handle (vi, CHR62000_ATTR_VOLTAGE, flag_plus, IVI_VAL_NEVER_CACHE)); 
		checkErr( chr62000_AttrFlags_Handle (vi, CHR62000_ATTR_CURRENT, flag_plus, IVI_VAL_NEVER_CACHE));  
	}
	else if (value == 0) 
	{
		checkErr( chr62000_AttrFlags_Handle (vi, CHR62000_ATTR_VOLTAGE, flag_minus, IVI_VAL_NEVER_CACHE)); 
		checkErr( chr62000_AttrFlags_Handle (vi, CHR62000_ATTR_CURRENT, flag_minus, IVI_VAL_NEVER_CACHE));
		checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_VOLTAGE)); 
		checkErr( Ivi_InvalidateAttribute (vi, VI_NULL, CHR62000_ATTR_CURRENT));  
	}	
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrApgReferenceVoltage_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[5];
	
	viCheckErr( viPrintf (io, "CONF:APGV?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"5V") == 0)
		*value=VI_FALSE;
	else if(stricmp(state,"10V") == 0)
		*value=VI_TRUE;		
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrApgReferenceVoltage_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean value)
{                                                                        
	ViStatus	error = VI_SUCCESS;
	
	if (value == VI_FALSE)
	{
		viCheckErr( viPrintf (io, "CONF:APGV FIVE\n"));
		Delay (delay_time);
	}
	else if (value == VI_TRUE)
	{
		viCheckErr( viPrintf (io, "CONF:APGV TEN\n"));
		Delay (delay_time);
	}
	
Error:
	return error;													   
}									  

static ViStatus _VI_FUNC chr62000AttrProgSelected_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEL?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSelected_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEL %d\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltage_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEQ:VOLT?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltage_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 2); 
	viCheckErr( viPrintf (io, "PROG:SEQ:VOLT %.2f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltSlew_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEQ:VOLT:SLEW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltSlew_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 3); 
	viCheckErr( viPrintf (io, "PROG:SEQ:VOLT:SLEW %.3f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrent_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEQ:CURR?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrent_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 2); 
	viCheckErr( viPrintf (io, "PROG:SEQ:CURR %.2f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrSlew_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		rdBuffer[20];
	
	viCheckErr( viPrintf (io, "PROG:SEQ:CURR:SLEW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", rdBuffer));
	if(stricmp(rdBuffer,Slew_INF) == 0)  
		*value=atof (ASCII_INF); 
	else
		*value=atof (rdBuffer);
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrSlew_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	if (value==0)
		viCheckErr( viPrintf (io, "PROG:SEQ:CURR:SLEWINF ENABLE\n"));
	
	else
	{
		chr62000_Digit_boundary (&value, 3); 
		viCheckErr( viPrintf (io, "PROG:SEQ:CURR:SLEW %.3f\n", value));
	}
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqTtl_ReadCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEQ:TTL?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqTtl_WriteCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEQ:TTL %d\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqTime_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEQ:TIME?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqTime_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 3); 
	viCheckErr( viPrintf (io, "PROG:SEQ:TIME %.3f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqType_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[20];
	
	viCheckErr( viPrintf (io, "PROG:SEQ:TYPE?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"AUTO") == 0)
		*value=0;
	else if(stricmp(state,"MANUAL") == 0)
		*value=1;
	else if(stricmp(state,"EXT.TRIGGER") == 0)
		*value=2;
	else if(stricmp(state,"SKIP") == 0)
		*value=3;
			
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqType_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	
	checkErr( Ivi_GetViInt32EntryFromIndex (value, &attrProgSeqTypeRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	viCheckErr( viPrintf (io, "PROG:SEQ:TYPE %s\n", Cmd));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgLink_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:LINK?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgLink_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:LINK %d\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgCount_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:COUNT?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgCount_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:COUNT %d\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqSelected_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEQ:SEL?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqSelected_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:SEQ:SEL %d\n", value));
	Delay (delay_time);
	
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgRun_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	if ( value == VI_TRUE)
	{
		viCheckErr( viPrintf (io, "PROG:RUN ON\n"));
		Delay (delay_time);
		checkErr( chr62000_Detect_error (vi, "PROG:RUN ON\n"));
	}
	else if ( value == VI_FALSE)
	{
		viCheckErr( viPrintf (io, "PROG:RUN OFF\n"));
		Delay (delay_time);
		checkErr( chr62000_Detect_error (vi, "PROG:RUN OFF\n"));
	}	
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrStatusEse_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "*ESE?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrStatusEse_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "*ESE %d\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrStatusSre_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "*SRE?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrStatusSre_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "*SRE %d\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrMstslvId_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[20];
	
	viCheckErr( viPrintf (io, "CONF:MSTSLV:ID?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"MASTER") == 0)
		*value=0;
	else if(stricmp(state,"SLAVE1") == 0)
		*value=1;
	else if(stricmp(state,"SLAVE2") == 0)
		*value=2;
	else if(stricmp(state,"SLAVE3") == 0)
		*value=3;		
	else if(stricmp(state,"SLAVE4") == 0)
		*value=4;		
		
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrMstslvId_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	
	checkErr( Ivi_GetViInt32EntryFromIndex (value, &attrMstslvIdRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	viCheckErr( viPrintf (io, "CONF:MSTSLV:ID %s\n", Cmd));
	Delay (delay_time);	

Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrMasterEnable_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[8];
	
	viCheckErr( viPrintf (io, "CONF:MSTSLV?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,ON_STRING) == 0)
		*value=VI_TRUE;
	else if(stricmp(state,OFF_STRING) == 0)
		*value=VI_FALSE;
			
Error:
	return error;	
}

static ViStatus _VI_FUNC chr62000AttrMasterEnable_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	if ( value == VI_TRUE)
	{
		viCheckErr( viPrintf (io, "CONF:MSTSLV ON\n"));
		Delay (delay_time);
	}
	else if ( value == VI_FALSE)
	{
		viCheckErr( viPrintf (io, "CONF:MSTSLV OFF\n"));
		Delay (delay_time);
	}	
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrParallelSeries_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[20];
	
	viCheckErr( viPrintf (io, "CONF:MSTSLV:PARSER?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"SERIES") == 0)
		*value=VI_TRUE;
	else if(stricmp(state,"PARALLEL") == 0)
		*value=VI_FALSE;
			
Error:
	return error;	
}

static ViStatus _VI_FUNC chr62000AttrParallelSeries_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	if ( value == VI_TRUE)
	{
		viCheckErr( viPrintf (io, "CONF:MSTSLV:PARSER SERIES\n"));
		Delay (delay_time);
	}
	else if ( value == VI_FALSE)
	{
		viCheckErr( viPrintf (io, "CONF:MSTSLV:PARSER PARALLEL\n"));
		Delay (delay_time);
	}	
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrSlaveNumbers_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:MSTSLV:NUMSLV?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrSlaveNumbers_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:MSTSLV:NUMSLV %d\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrSlewInf_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	if ( value == VI_TRUE)
	{
		viCheckErr( viPrintf (io, "SOUR:CURR:SLEWINF ENABLE\n"));
		Delay (delay_time);
	}
	else if ( value == VI_FALSE)
	{
		viCheckErr( viPrintf (io, "SOUR:CURR:SLEWINF DISABLE\n"));
		Delay (delay_time);
	}	
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVdcOnFall_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:DCON:FALL?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVdcOnFall_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 1); 
	viCheckErr( viPrintf (io, "SOUR:DCON:FALL %.1f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVdcOnRise_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:DCON:RISE?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVdcOnRise_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 1); 
	viCheckErr( viPrintf (io, "SOUR:DCON:RISE %.1f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltMin_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:LIM:LOW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltMin_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;

	viCheckErr( viPrintf (io, "SOUR:VOLT:LIM:LOW MIN\n", value));
	Delay (delay_time);
	
Error:
	return error;
}



static ViStatus _VI_FUNC chr62000AttrVoltMax_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:LIM:HIGH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltMax_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:LIM:HIGH MAX\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltage_RangeTableCallback (ViSession vi,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrMax_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR:LIM:HIGH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrMax_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR:LIM:HIGH MAX\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrMin_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR:LIM:LOW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrMin_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR:LIM:LOW MIN\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrIslewMax_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		rdBuffer[20];
	
	viCheckErr( viPrintf (io, "SOUR:CURR:SLEW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", rdBuffer));
	if(stricmp(rdBuffer,Slew_INF) == 0)  
		*value=atof (ASCII_INF); 
	else
		*value=atof (rdBuffer);
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrIslewMax_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR:SLEW MAX\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrIslewMin_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		rdBuffer[20];
	
	viCheckErr( viPrintf (io, "SOUR:CURR:SLEW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", rdBuffer));
	if(stricmp(rdBuffer,Slew_INF) == 0)  
		*value=atof (ASCII_INF); 
	else
		*value=atof (rdBuffer);
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrIslewMin_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:CURR:SLEW MIN\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrPwrMax_ReadCallback (ViSession vi,
                                                          ViSession io,
                                                          ViConstString channelName,
                                                          ViAttr attributeId,
                                                          ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:POW:PROT:HIGH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrPwrMax_WriteCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;

	viCheckErr( viPrintf (io, "SOUR:POW:PROT:HIGH MAX\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrPwrMin_ReadCallback (ViSession vi,
                                                          ViSession io,
                                                          ViConstString channelName,
                                                          ViAttr attributeId,
                                                          ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:POW:PROT:HIGH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrPwrMin_WriteCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;

	viCheckErr( viPrintf (io, "SOUR:POW:PROT:HIGH MIN\n", value));
	Delay (delay_time);
	
Error:
	return error;
}



static ViStatus _VI_FUNC chr62000AttrVdcFMin_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:DCON:FALL?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVdcFMin_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:DCON:FALL MIN\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVdcRMax_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:DCON:RISE?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVdcRMax_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 1); 
	viCheckErr( viPrintf (io, "SOUR:DCON:RISE MAX\n", value));
	Delay (delay_time);
	
Error:
	return error;
}


static ViStatus _VI_FUNC chr62000AttrVslewMax_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:SLEW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVslewMax_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:SLEW MAX\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVslewMin_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:SLEW?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVslewMin_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "SOUR:VOLT:SLEW MIN\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltLimitHigh_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltLimitLow_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltProtect_RangeTableCallback (ViSession vi,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max*OVP_RATIO, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVdcOnFall_RangeTableCallback (ViSession vi,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_R_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_F_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVdcOnRise_RangeTableCallback (ViSession vi,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_R_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VDC_F_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrVoltSlew_RangeTableCallback (ViSession vi,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrLimitHigh_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrLimitLow_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrProtect_RangeTableCallback (ViSession vi,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max*OCP_RATIO, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrSlew_RangeTableCallback (ViSession vi,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_ISLEW_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_ISLEW_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrCurrent_RangeTableCallback (ViSession vi,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrPowerProtect_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PWR_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_PWR_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrBacklight_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[8];
	
	viCheckErr( viPrintf (io, "CONF:BACKL?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"OFF") == 0)
		*value=0;
	else if(stricmp(state,"DIM") == 0)
		*value=1;		
	else if(stricmp(state,"NOR") == 0)
		*value=2;
	else if(stricmp(state,"HIGH") == 0)
		*value=3;	
		
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrBacklight_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	
	checkErr( Ivi_GetViInt32EntryFromIndex (value, &attrBacklightRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	viCheckErr( viPrintf (io, "CONF:BACKL %s\n", Cmd));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrMeasspd_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:MEAS:SP?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrMeasspd_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	
	checkErr( Ivi_GetViInt32EntryFromIndex (value, &attrMeasspdRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	viCheckErr( viPrintf (io, "CONF:MEAS:SP %s\n", Cmd));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrAvgMethod_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[8];
	
	viCheckErr( viPrintf (io, "CONF:AVG:METH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"MOV") == 0)
		*value=VI_TRUE;
	else if(stricmp(state,"FIX") == 0)
		*value=VI_FALSE;
			
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrAvgMethod_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	if ( value == VI_TRUE)
	{
		viCheckErr( viPrintf (io, "CONF:AVG:METH MOV\n"));
		Delay (delay_time);
	}
	else if ( value == VI_FALSE)
	{
		viCheckErr( viPrintf (io, "CONF:AVG:METH FIX\n"));
		Delay (delay_time);
	}	
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrAvgTimes_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	
	checkErr( Ivi_GetViInt32EntryFromIndex (value, &attrAvgTimesRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	viCheckErr( viPrintf (io, "CONF:AVG:TIMES %s\n", Cmd));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrInhibit_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	
	checkErr( Ivi_GetViInt32EntryFromIndex (value, &attrInhibitRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	viCheckErr( viPrintf (io, "CONF:INH %s\n", Cmd));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrInhibit_ReadCallback (ViSession vi,
                                                           ViSession io,
                                                           ViConstString channelName,
                                                           ViAttr attributeId,
                                                           ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[8];
	
	viCheckErr( viPrintf (io, "CONF:INH?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"OFF") == 0)
		*value=0;
	else if(stricmp(state,"TRIG") == 0)
		*value=1;		
	else if(stricmp(state,"LIVE") == 0)
		*value=2;

Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrent_RangeTableCallback (ViSession vi,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_CURR_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqCurrSlew_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_ISLEW_MAX, 0, &Max); 
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, prog_curr_slew_inf, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltSlew_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VSLEW_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqVoltage_RangeTableCallback (ViSession vi,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgMode_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViChar		state[8];
	
	viCheckErr( viPrintf (io, "PROG:MODE?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%s", state)); 
	Delay (delay_time);
	
	if(stricmp(state,"LIST") == 0)
		*value=0;
	else if(stricmp(state,"STEP") == 0)
		*value=1;		
		
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgMode_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString	Cmd;
	
	checkErr( Ivi_GetViInt32EntryFromIndex (value, &attrProgModeRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &Cmd, VI_NULL));
	viCheckErr( viPrintf (io, "PROG:MODE %s\n", Cmd));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgStepTime_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:STEP:TIME?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgStepTime_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		HH=0, MM=0;
	ViReal64	SS=0.00;
	
	chr62000_Digit_boundary (&value, 2);
	HH= value/3600.00;
	MM= (value-(float)(HH*3600))/60.00;
	SS= (value-(float)(HH*3600))-(float)(MM*60);
	
	viCheckErr( viPrintf (io, "PROG:STEP:TIME %d,%d,%.2f\n", HH, MM, SS));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgStepStartv_RangeTableCallback (ViSession vi,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgStepStartv_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:STEP:STARTV?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgStepStartv_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 2); 
	viCheckErr( viPrintf (io, "PROG:STEP:STARTV %.2f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgStepEndv_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64	Min=0,Max=0;

	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MAX, 0, &Max); 
	Ivi_GetAttributeViReal64 (vi, VI_NULL, CHR62000_ATTR_VOLT_MIN, 0, &Min);
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, Min, Max, &tblPtr));

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgStepEndv_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:STEP:ENDV?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%Lf", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgStepEndv_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
	chr62000_Digit_boundary (&value, 2); 
	viCheckErr( viPrintf (io, "PROG:STEP:ENDV %.2f\n", value));
	Delay (delay_time);
	
Error:
	return error;
}


static ViStatus _VI_FUNC chr62000AttrProgAdd_RangeTableCallback (ViSession vi,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32  	RestSeq;

	Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR62000_ATTR_PROG_SEQ_REST, 0, &RestSeq); 
	
	checkErr( chr62000_SwapAttributeViReal64MinMax1 (vi, attributeId, 1, RestSeq, &tblPtr));
	
Error:																							  //add by justin.hsu 12/03/08 
	*rangeTablePtr = tblPtr;
	return error;
}
static ViStatus _VI_FUNC chr62000AttrProgAdd_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:ADD %d\n", value));
	Delay (delay_time);
	
Error:
	return error;
}

static ViStatus _VI_FUNC chr62000AttrProgSeqRest_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "PROG:ADD?\n"));
	Delay (delay_time);
	viCheckErr( viScanf (io, "%d", value)); 
	Delay (delay_time);
	
Error:
	return error;
}

/*****************************************************************************
 * Function: chr62000_InitAttributes                                                  
 * Purpose:  This function adds attributes to the IVI session, initializes   
 *           instrument attributes, and sets attribute invalidation          
 *           dependencies.                                                   
 *****************************************************************************/
static ViStatus chr62000_InitAttributes (ViSession vi)
{
    ViStatus    error = VI_SUCCESS;
    ViInt32     flags = 0;
    
        /*- Initialize instrument attributes --------------------------------*/            

    checkErr( Ivi_SetAttributeViInt32 (vi, "", CHR62000_ATTR_SPECIFIC_DRIVER_MAJOR_VERSION,
                                       0, CHR62000_MAJOR_VERSION));
    checkErr( Ivi_SetAttributeViInt32 (vi, "", CHR62000_ATTR_SPECIFIC_DRIVER_MINOR_VERSION,
                                       0, CHR62000_MINOR_VERSION));
    checkErr( Ivi_SetAttrReadCallbackViString (vi, CHR62000_ATTR_SPECIFIC_DRIVER_REVISION,
                                               chr62000AttrDriverRevision_ReadCallback));
    checkErr( Ivi_SetAttributeViInt32 (vi, "", CHR62000_ATTR_SPECIFIC_DRIVER_CLASS_SPEC_MAJOR_VERSION,
                                       0, CHR62000_CLASS_SPEC_MAJOR_VERSION));
    checkErr( Ivi_SetAttributeViInt32 (vi, "", CHR62000_ATTR_SPECIFIC_DRIVER_CLASS_SPEC_MINOR_VERSION,
                                       0, CHR62000_CLASS_SPEC_MINOR_VERSION));
    checkErr( Ivi_SetAttributeViString (vi, "", CHR62000_ATTR_IO_SESSION_TYPE,
                                        0, CHR62000_IO_SESSION_TYPE));
    checkErr( Ivi_SetAttributeViString (vi, "", CHR62000_ATTR_SUPPORTED_INSTRUMENT_MODELS,
                                        0, CHR62000_SUPPORTED_INSTRUMENT_MODELS));


    checkErr( Ivi_SetAttrReadCallbackViString (vi, CHR62000_ATTR_INSTRUMENT_FIRMWARE_REVISION,
                                               chr62000AttrFirmwareRevision_ReadCallback));

	checkErr( Ivi_GetAttributeFlags (vi, CHR62000_ATTR_INSTRUMENT_MANUFACTURER, &flags));
	checkErr( Ivi_SetAttributeFlags (vi, CHR62000_ATTR_INSTRUMENT_MANUFACTURER, 
	                                 flags | IVI_VAL_USE_CALLBACKS_FOR_SIMULATION));
	checkErr( Ivi_SetAttrReadCallbackViString (vi, CHR62000_ATTR_INSTRUMENT_MANUFACTURER,
	                                           chr62000AttrInstrumentManufacturer_ReadCallback));

	checkErr( Ivi_GetAttributeFlags (vi, CHR62000_ATTR_INSTRUMENT_MODEL, &flags));
	checkErr( Ivi_SetAttributeFlags (vi, CHR62000_ATTR_INSTRUMENT_MODEL, 
	                                 flags | IVI_VAL_USE_CALLBACKS_FOR_SIMULATION));
	checkErr( Ivi_SetAttrReadCallbackViString (vi, CHR62000_ATTR_INSTRUMENT_MODEL,
	                                           chr62000AttrInstrumentModel_ReadCallback));

    checkErr( Ivi_SetAttributeViString (vi, "", CHR62000_ATTR_SPECIFIC_DRIVER_VENDOR,
                                        0, CHR62000_DRIVER_VENDOR));
    checkErr( Ivi_SetAttributeViString (vi, "", CHR62000_ATTR_SPECIFIC_DRIVER_DESCRIPTION,
                                        0, CHR62000_DRIVER_DESCRIPTION));
    checkErr( Ivi_SetAttributeViAddr (vi, VI_NULL, IVI_ATTR_OPC_CALLBACK, 0,
                                      chr62000_WaitForOPCCallback));
    checkErr( Ivi_SetAttributeViAddr (vi, VI_NULL, IVI_ATTR_CHECK_STATUS_CALLBACK, 0,
                                      chr62000_CheckStatusCallback));
	checkErr( Ivi_SetAttributeViBoolean (vi, VI_NULL, IVI_ATTR_SUPPORTS_WR_BUF_OPER_MODE, 
	                                     0, VI_TRUE));

        
	checkErr( Ivi_SetAttributeViString (vi, "", CHR62000_ATTR_GROUP_CAPABILITIES, 0,
	                                    "None"));
    
        /*- Add instrument-specific attributes ------------------------------*/            
	
	checkErr (Ivi_AddAttributeViString (vi, CHR62000_ATTR_ID_QUERY_RESPONSE,
	                                    "CHR62000_ATTR_ID_QUERY_RESPONSE",
	                                    "CHROMA,62012P-80-60,01.00,2005/08/16",
	                                    IVI_VAL_NOT_USER_WRITABLE,
	                                    chr62000AttrIdQueryResponse_ReadCallback,
	                                    VI_NULL));
	                                           
    checkErr( Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_OPC_TIMEOUT,
                                       "CHR62000_ATTR_OPC_TIMEOUT",
                                       5000, IVI_VAL_HIDDEN | IVI_VAL_DONT_CHECK_STATUS,
                                       VI_NULL, VI_NULL, VI_NULL)); 
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VOLTAGE,
	                                    "CHR62000_ATTR_VOLTAGE", 0.00, 0,
	                                    chr62000AttrVoltage_ReadCallback,
	                                    chr62000AttrVoltage_WriteCallback,
	                                    &attrVoltageRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_VOLTAGE,
	                                         chr62000AttrVoltage_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VOLT_LIMIT_HIGH,
	                                    "CHR62000_ATTR_VOLT_LIMIT_HIGH", 80.0, 0,
	                                    chr62000AttrVoltLimitHigh_ReadCallback,
	                                    chr62000AttrVoltLimitHigh_WriteCallback,
	                                    &attrVoltLimitHighRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_VOLT_LIMIT_HIGH,
	                                         chr62000AttrVoltLimitHigh_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VOLT_LIMIT_LOW,
	                                    "CHR62000_ATTR_VOLT_LIMIT_LOW", 0.0, 0,
	                                    chr62000AttrVoltLimitLow_ReadCallback,
	                                    chr62000AttrVoltLimitLow_WriteCallback,
	                                    &attrVoltLimitLowRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_VOLT_LIMIT_LOW,
	                                         chr62000AttrVoltLimitLow_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VOLT_PROTECT,
	                                    "CHR62000_ATTR_VOLT_PROTECT", 88.0, 0,
	                                    chr62000AttrVoltProtect_ReadCallback,
	                                    chr62000AttrVoltProtect_WriteCallback,
	                                    &attrVoltProtectRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_VOLT_PROTECT,
	                                         chr62000AttrVoltProtect_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VOLT_SLEW,
	                                    "CHR62000_ATTR_VOLT_SLEW", 1.00, 0,
	                                    chr62000AttrVoltSlew_ReadCallback,
	                                    chr62000AttrVoltSlew_WriteCallback,
	                                    &attrVoltSlewRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_VOLT_SLEW,
	                                         chr62000AttrVoltSlew_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_CURRENT,
	                                    "CHR62000_ATTR_CURRENT", 0.00, 0,
	                                    chr62000AttrCurrent_ReadCallback,
	                                    chr62000AttrCurrent_WriteCallback,
	                                    &attrCurrentRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_CURRENT,
	                                         chr62000AttrCurrent_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_CURR_LIMIT_HIGH,
	                                    "CHR62000_ATTR_CURR_LIMIT_HIGH", 60.0, 0,
	                                    chr62000AttrCurrLimitHigh_ReadCallback,
	                                    chr62000AttrCurrLimitHigh_WriteCallback,
	                                    &attrCurrLimitHighRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_CURR_LIMIT_HIGH,
	                                         chr62000AttrCurrLimitHigh_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_CURR_LIMIT_LOW,
	                                    "CHR62000_ATTR_CURR_LIMIT_LOW", 0.0, 0,
	                                    chr62000AttrCurrLimitLow_ReadCallback,
	                                    chr62000AttrCurrLimitLow_WriteCallback,
	                                    &attrCurrLimitLowRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_CURR_LIMIT_LOW,
	                                         chr62000AttrCurrLimitLow_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_CURR_PROTECT,
	                                    "CHR62000_ATTR_CURR_PROTECT", 63.0, 0,
	                                    chr62000AttrCurrProtect_ReadCallback,
	                                    chr62000AttrCurrProtect_WriteCallback,
	                                    &attrCurrProtectRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_CURR_PROTECT,
	                                         chr62000AttrCurrProtect_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_CURR_SLEW,
	                                    "CHR62000_ATTR_CURR_SLEW", 1.00, 0,
	                                    chr62000AttrCurrSlew_ReadCallback,
	                                    chr62000AttrCurrSlew_WriteCallback,
	                                    &attrCurrSlewRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_CURR_SLEW,
	                                         chr62000AttrCurrSlew_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_POWER_PROTECT,
	                                    "CHR62000_ATTR_POWER_PROTECT", 1260.0, 0,
	                                    chr62000AttrPowerProtect_ReadCallback,
	                                    chr62000AttrPowerProtect_WriteCallback,
	                                    &attrPowerProtectRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_POWER_PROTECT,
	                                         chr62000AttrPowerProtect_RangeTableCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR62000_ATTR_OUTPUT,
	                                     "CHR62000_ATTR_OUTPUT", VI_FALSE,
	                                     IVI_VAL_HIDDEN, VI_NULL,
	                                     chr62000AttrOutput_WriteCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_TTL_PORT,
	                                   "CHR62000_ATTR_TTL_PORT", 0, 0,
	                                   chr62000AttrTtlPort_ReadCallback,
	                                   chr62000AttrTtlPort_WriteCallback,
	                                   &attrTtlPortRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR62000_ATTR_BEEPER,
	                                     "CHR62000_ATTR_BEEPER", VI_TRUE,
	                                     IVI_VAL_HIDDEN,
	                                     chr62000AttrBeeper_ReadCallback,
	                                     chr62000AttrBeeper_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR62000_ATTR_REMOTE_MODE,
	                                     "CHR62000_ATTR_REMOTE_MODE", VI_FALSE, 0,
	                                     VI_NULL,
	                                     chr62000AttrRemoteMode_WriteCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_FOLDBACK_PROTECT,
	                                   "CHR62000_ATTR_FOLDBACK_PROTECT",
	                                   CHR62000_VAL_FOLD_DISABLE, IVI_VAL_HIDDEN,
	                                   chr62000AttrFoldbackProtect_ReadCallback,
	                                   chr62000AttrFoldbackProtect_WriteCallback,
	                                   &attrFoldbackProtectRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_FOLDBACK_DELAY_TIME,
	                                    "CHR62000_ATTR_FOLDBACK_DELAY_TIME", 0.01, 0,
	                                    chr62000AttrFoldbackDelayTime_ReadCallback,
	                                    chr62000AttrFoldbackDelayTime_WriteCallback,
	                                    &attrFoldbackDelayTimeRangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_APG_MODE,
	                                   "CHR62000_ATTR_APG_MODE",
	                                   CHR62000_VAL_APG_NONE, IVI_VAL_HIDDEN,
	                                   chr62000AttrApgMode_ReadCallback,
	                                   chr62000AttrApgMode_WriteCallback,
	                                   &attrApgModeRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR62000_ATTR_APG_REFERENCE_VOLTAGE,
	                                     "CHR62000_ATTR_APG_REFERENCE_VOLTAGE",
	                                     VI_FALSE, IVI_VAL_HIDDEN,
	                                     chr62000AttrApgReferenceVoltage_ReadCallback,
	                                     chr62000AttrApgReferenceVoltage_WriteCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_PROG_SELECTED,
	                                   "CHR62000_ATTR_PROG_SELECTED", 1, 0,
	                                   chr62000AttrProgSelected_ReadCallback,
	                                   chr62000AttrProgSelected_WriteCallback,
	                                   &attrProgSelectedRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PROG_SEQ_VOLTAGE,
	                                    "CHR62000_ATTR_PROG_SEQ_VOLTAGE", 0.00,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_HIDDEN,
	                                    chr62000AttrProgSeqVoltage_ReadCallback,
	                                    chr62000AttrProgSeqVoltage_WriteCallback,
	                                    &attrProgSeqVoltageRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_PROG_SEQ_VOLTAGE,
	                                         chr62000AttrProgSeqVoltage_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PROG_SEQ_VOLT_SLEW,
	                                    "CHR62000_ATTR_PROG_SEQ_VOLT_SLEW", 10.00,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_HIDDEN,
	                                    chr62000AttrProgSeqVoltSlew_ReadCallback,
	                                    chr62000AttrProgSeqVoltSlew_WriteCallback,
	                                    &attrProgSeqVoltSlewRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_PROG_SEQ_VOLT_SLEW,
	                                         chr62000AttrProgSeqVoltSlew_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PROG_SEQ_CURRENT,
	                                    "CHR62000_ATTR_PROG_SEQ_CURRENT", 0.00,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_HIDDEN,
	                                    chr62000AttrProgSeqCurrent_ReadCallback,
	                                    chr62000AttrProgSeqCurrent_WriteCallback,
	                                    &attrProgSeqCurrentRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_PROG_SEQ_CURRENT,
	                                         chr62000AttrProgSeqCurrent_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PROG_SEQ_CURR_SLEW,
	                                    "CHR62000_ATTR_PROG_SEQ_CURR_SLEW", 1.00,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_HIDDEN,
	                                    chr62000AttrProgSeqCurrSlew_ReadCallback,
	                                    chr62000AttrProgSeqCurrSlew_WriteCallback,
	                                    &attrProgSeqCurrSlewRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_PROG_SEQ_CURR_SLEW,
	                                         chr62000AttrProgSeqCurrSlew_RangeTableCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_PROG_SEQ_TTL,
	                                   "CHR62000_ATTR_PROG_SEQ_TTL", 0,
	                                   IVI_VAL_MULTI_CHANNEL | IVI_VAL_HIDDEN,
	                                   chr62000AttrProgSeqTtl_ReadCallback,
	                                   chr62000AttrProgSeqTtl_WriteCallback,
	                                   &attrProgSeqTtlRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PROG_SEQ_TIME,
	                                    "CHR62000_ATTR_PROG_SEQ_TIME", 0.00,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_HIDDEN,
	                                    chr62000AttrProgSeqTime_ReadCallback,
	                                    chr62000AttrProgSeqTime_WriteCallback,
	                                    &attrProgSeqTimeRangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_PROG_SEQ_TYPE,
	                                   "CHR62000_ATTR_PROG_SEQ_TYPE",
	                                   CHR62000_VAL_AUTO,
	                                   IVI_VAL_MULTI_CHANNEL | IVI_VAL_HIDDEN,
	                                   chr62000AttrProgSeqType_ReadCallback,
	                                   chr62000AttrProgSeqType_WriteCallback,
	                                   &attrProgSeqTypeRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_PROG_LINK,
	                                   "CHR62000_ATTR_PROG_LINK", 0,
	                                   IVI_VAL_MULTI_CHANNEL | IVI_VAL_HIDDEN,
	                                   chr62000AttrProgLink_ReadCallback,
	                                   chr62000AttrProgLink_WriteCallback,
	                                   &attrProgLinkRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_PROG_COUNT,
	                                   "CHR62000_ATTR_PROG_COUNT", 1,
	                                   IVI_VAL_MULTI_CHANNEL | IVI_VAL_HIDDEN,
	                                   chr62000AttrProgCount_ReadCallback,
	                                   chr62000AttrProgCount_WriteCallback,
	                                   &attrProgCountRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_PROG_SEQ_SELECTED,
	                                   "CHR62000_ATTR_PROG_SEQ_SELECTED", 1, 0,
	                                   chr62000AttrProgSeqSelected_ReadCallback,
	                                   chr62000AttrProgSeqSelected_WriteCallback,
	                                   &attrProgSeqSelectedRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR62000_ATTR_PROG_RUN,
	                                     "CHR62000_ATTR_PROG_RUN", VI_FALSE,
	                                     IVI_VAL_NEVER_CACHE | IVI_VAL_HIDDEN,
	                                     VI_NULL, chr62000AttrProgRun_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_MAXPOWER,
	                                    "CHR62000_ATTR_MAXPOWER", 1200.00,
	                                    IVI_VAL_HIDDEN, VI_NULL, VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_STATUS_ESE,
	                                   "CHR62000_ATTR_STATUS_ESE", 0, 0,
	                                   chr62000AttrStatusEse_ReadCallback,
	                                   chr62000AttrStatusEse_WriteCallback,
	                                   &attrStatusEseRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_STATUS_SRE,
	                                   "CHR62000_ATTR_STATUS_SRE", 0, 0,
	                                   chr62000AttrStatusSre_ReadCallback,
	                                   chr62000AttrStatusSre_WriteCallback,
	                                   &attrStatusSreRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_SIMU_RESISTOR,
	                                    "CHR62000_ATTR_SIMU_RESISTOR", 1000.00,
	                                    IVI_VAL_HIDDEN, VI_NULL, VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_MSTSLV_ID,
	                                   "CHR62000_ATTR_MSTSLV_ID",
	                                   CHR62000_VAL_MASTER, IVI_VAL_HIDDEN,
	                                   chr62000AttrMstslvId_ReadCallback,
	                                   chr62000AttrMstslvId_WriteCallback,
	                                   &attrMstslvIdRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR62000_ATTR_PARALLEL_SERIES,
	                                     "CHR62000_ATTR_PARALLEL_SERIES", VI_FALSE,
	                                     IVI_VAL_HIDDEN,
	                                     chr62000AttrParallelSeries_ReadCallback,
	                                     chr62000AttrParallelSeries_WriteCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_SLAVE_NUMBERS,
	                                   "CHR62000_ATTR_SLAVE_NUMBERS", 1,
	                                   IVI_VAL_NOT_USER_WRITABLE,
	                                   chr62000AttrSlaveNumbers_ReadCallback,
	                                   chr62000AttrSlaveNumbers_WriteCallback,
	                                   &attrSlaveNumbersRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR62000_ATTR_MASTER_ENABLE,
	                                     "CHR62000_ATTR_MASTER_ENABLE", VI_FALSE,
	                                     IVI_VAL_HIDDEN,
	                                     chr62000AttrMasterEnable_ReadCallback,
	                                     chr62000AttrMasterEnable_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR62000_ATTR_CURR_SLEW_INF,
	                                     "CHR62000_ATTR_CURR_SLEW_INF", VI_TRUE,
	                                     IVI_VAL_HIDDEN, VI_NULL,
	                                     chr62000AttrCurrSlewInf_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VDC_ON_RISE,
	                                    "CHR62000_ATTR_VDC_ON_RISE", 0.5, 0,
	                                    chr62000AttrVdcOnRise_ReadCallback,
	                                    chr62000AttrVdcOnRise_WriteCallback,
	                                    &attrVdcOnRiseRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_VDC_ON_RISE,
	                                         chr62000AttrVdcOnRise_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VDC_ON_FALL,
	                                    "CHR62000_ATTR_VDC_ON_FALL", 0.5, 0,
	                                    chr62000AttrVdcOnFall_ReadCallback,
	                                    chr62000AttrVdcOnFall_WriteCallback,
	                                    &attrVdcOnFallRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_VDC_ON_FALL,
	                                         chr62000AttrVdcOnFall_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VOLT_MIN,
	                                    "CHR62000_ATTR_VOLT_MIN", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrVoltMin_ReadCallback,
	                                    chr62000AttrVoltMin_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VOLT_MAX,
	                                    "CHR62000_ATTR_VOLT_MAX", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrVoltMax_ReadCallback,
	                                    chr62000AttrVoltMax_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_CURR_MAX,
	                                    "CHR62000_ATTR_CURR_MAX", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrCurrMax_ReadCallback,
	                                    chr62000AttrCurrMax_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_CURR_MIN,
	                                    "CHR62000_ATTR_CURR_MIN", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrCurrMin_ReadCallback,
	                                    chr62000AttrCurrMin_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PWR_MAX,
	                                    "CHR62000_ATTR_PWR_MAX", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrPwrMax_ReadCallback,
	                                    chr62000AttrPwrMax_WriteCallback, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PWR_MIN,
	                                    "CHR62000_ATTR_PWR_MIN", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrPwrMin_ReadCallback,
	                                    chr62000AttrPwrMin_WriteCallback, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VSLEW_MAX,
	                                    "CHR62000_ATTR_VSLEW_MAX", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrVslewMax_ReadCallback,
	                                    chr62000AttrVslewMax_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VSLEW_MIN,
	                                    "CHR62000_ATTR_VSLEW_MIN", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrVslewMin_ReadCallback,
	                                    chr62000AttrVslewMin_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_ISLEW_MAX,
	                                    "CHR62000_ATTR_ISLEW_MAX", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrIslewMax_ReadCallback,
	                                    chr62000AttrIslewMax_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_ISLEW_MIN,
	                                    "CHR62000_ATTR_ISLEW_MIN", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrIslewMin_ReadCallback,
	                                    chr62000AttrIslewMin_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VDC_R_MAX,
	                                    "CHR62000_ATTR_VDC_R_MAX", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrVdcRMax_ReadCallback,
	                                    chr62000AttrVdcRMax_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_VDC_F_MIN,
	                                    "CHR62000_ATTR_VDC_F_MIN", 0,
	                                    IVI_VAL_ALWAYS_CACHE | IVI_VAL_HIDDEN,
	                                    chr62000AttrVdcFMin_ReadCallback,
	                                    chr62000AttrVdcFMin_WriteCallback, VI_NULL,
	                                    0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_BACKLIGHT,
	                                   "CHR62000_ATTR_BACKLIGHT",
	                                   CHR62000_VAL_BACKL_HIGH, IVI_VAL_HIDDEN,
	                                   chr62000AttrBacklight_ReadCallback,
	                                   chr62000AttrBacklight_WriteCallback,
	                                   &attrBacklightRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_MEASSPD,
	                                   "CHR62000_ATTR_MEASSPD",
	                                   CHR62000_VAL_MEASSPD_60, IVI_VAL_HIDDEN,
	                                   chr62000AttrMeasspd_ReadCallback,
	                                   chr62000AttrMeasspd_WriteCallback,
	                                   &attrMeasspdRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR62000_ATTR_AVG_METHOD,
	                                     "CHR62000_ATTR_AVG_METHOD", VI_FALSE, 0,
	                                     chr62000AttrAvgMethod_ReadCallback,
	                                     chr62000AttrAvgMethod_WriteCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_AVG_TIMES,
	                                   "CHR62000_ATTR_AVG_TIMES",
	                                   CHR62000_VAL_AVG_TIMES_1, IVI_VAL_HIDDEN,
	                                   VI_NULL, chr62000AttrAvgTimes_WriteCallback,
	                                   &attrAvgTimesRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_INHIBIT,
	                                   "CHR62000_ATTR_INHIBIT",
	                                   CHR62000_VAL_INHIBIT_OFF, IVI_VAL_HIDDEN,
	                                   chr62000AttrInhibit_ReadCallback,
	                                   chr62000AttrInhibit_WriteCallback,
	                                   &attrInhibitRangeTable));  
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_PROG_MODE,
	                                   "CHR62000_ATTR_PROG_MODE",
	                                   CHR62000_VAL_PROG_LIST, IVI_VAL_HIDDEN,
	                                   chr62000AttrProgMode_ReadCallback,
	                                   chr62000AttrProgMode_WriteCallback,
	                                   &attrProgModeRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PROG_STEP_TIME,
	                                    "CHR62000_ATTR_PROG_STEP_TIME", 0.00,
	                                    IVI_VAL_HIDDEN,
	                                    chr62000AttrProgStepTime_ReadCallback,
	                                    chr62000AttrProgStepTime_WriteCallback,
	                                    &attrProgStepTimeRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PROG_STEP_STARTV,
	                                    "CHR62000_ATTR_PROG_STEP_STARTV", 0.00, 0,
	                                    chr62000AttrProgStepStartv_ReadCallback,
	                                    chr62000AttrProgStepStartv_WriteCallback,
	                                    &attrProgStepStartvRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR62000_ATTR_PROG_STEP_ENDV,
	                                    "CHR62000_ATTR_PROG_STEP_ENDV", 0.00, 0,
	                                    chr62000AttrProgStepEndv_ReadCallback,
	                                    chr62000AttrProgStepEndv_WriteCallback,
	                                    &attrProgStepEndvRangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_PROG_SEQ_REST,
	                                   "CHR62000_ATTR_PROG_SEQ_REST", 0,
	                                   IVI_VAL_NEVER_CACHE,
	                                   chr62000AttrProgSeqRest_ReadCallback, VI_NULL,
	                                   &attrProgSeqRestRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR62000_ATTR_PROG_ADD,
	                                   "CHR62000_ATTR_PROG_ADD", 0,
	                                   IVI_VAL_NEVER_CACHE, VI_NULL,
	                                   chr62000AttrProgAdd_WriteCallback,
	                                   &attrProgAddRangeTable));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_PROG_ADD,
	                                         chr62000AttrProgAdd_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_PROG_STEP_ENDV,
	                                         chr62000AttrProgStepEndv_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR62000_ATTR_PROG_STEP_STARTV,
	                                         chr62000AttrProgStepStartv_RangeTableCallback));
                                       
        /*- Setup attribute invalidations -----------------------------------*/            


	checkErr (Ivi_AddAttributeInvalidation (vi, CHR62000_ATTR_VOLTAGE,
	                                        CHR62000_ATTR_VOLT_PROTECT, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR62000_ATTR_CURRENT,
	                                        CHR62000_ATTR_CURR_PROTECT, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR62000_ATTR_CURR_SLEW_INF,
	                                        CHR62000_ATTR_CURR_SLEW, VI_FALSE));
Error:
    return error;
}

/*****************************************************************************
 *------------------- End Instrument Driver Source Code ---------------------*
 *****************************************************************************/

