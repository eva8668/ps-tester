#include "toolbox.h"
#include <utility.h>
#include <ansi_c.h>
#include <cvirte.h>		
#include <userint.h>
#include "chr6310A_example.h"
#include "chr62000.h"

// Constants
#define CTRL_ENABLED	0	// Not dimmed
#define CTRL_DISABLED	1	// Dimmed

// Function prototypes
int CVICALLBACK initSessionParams(void);
int CVICALLBACK abortApplication(ViStatus error);
void startMeasureThread();
int closeConnection();
void toggleComponents(int enable);
int CVICALLBACK fetchVolCurrThread(void *functionData);
int CVICALLBACK stepDynamicCurrThread(void *functionData);

// Global variables
static ViSession vi;
static int panelHandle;
volatile int quit = 0, quitStepThread = 0;
static CmtThreadFunctionID stepFunctionID, fetchFunctionID;

static int			mRadioID = 0;
static ViReal64		curr_dh, curr_dl, curr_0;
static double		t_dh, t_dl, t_0;

// Application entry point
int main (int argc, char *argv[])
{
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "chr6310A_example.uir", PANEL)) < 0)
		return -1;
	
	DisplayPanel (panelHandle);
	RunUserInterface ();
	DiscardPanel (panelHandle);
	return 0;
}

/********************************************************/
/* Callbacks of UI components                           */
/********************************************************/
int CVICALLBACK next(int panel, int control, int event,
                     void *callbackData, int eventData1, int eventData2)
{
	ViStatus	error = VI_SUCCESS; 
	int 		resource=0,gpib_addr=0,com_port=0,baudrate=0,simulate=0;
	char 		ResName[20]="";
	char 		ID[512]="", Model[512]="";
	
	switch (event) {
		case EVENT_COMMIT: {
			// Set up parameters
			resource = 1; // RS-232C protocol
			
			GetCtrlVal (panelHandle, PANEL_RING_COMPORT, &com_port);
			//com_port = 3; // COM port 3 (1-based)
			
			GetCtrlVal (panelHandle, PANEL_RING_BAUDRATE, &baudrate);
			//baudrate = 9600; // baudrate 9600 bits
			
			GetCtrlVal (panelHandle, PANEL_CB_SIMULATE, &simulate);
			//simulate = 1; // Use simulation for demo
			
			// Start to create protocol session
			if (resource==1)	
				sprintf (ResName,"ASRL%d::INSTR",com_port);
			else
				sprintf (ResName,"GPIB0::%d::INSTR",gpib_addr);
				
			if (simulate)
			{
				if ((chr62000_InitWithOptions (ResName, VI_TRUE, VI_FALSE,
                          "Simulate=1,RangeCheck=1,QueryInstrStatus=1,Cache=1", &vi)) !=0)
				{                
                    MessagePopup ("Simulation Error","Can not simulate CHROMA 62000P DC power supply.");
                    QuitUserInterface (0);
                    break;
                }    
            }              
			else
			{
				if ((chr62000_InitInterface (ResName, VI_TRUE, VI_FALSE, baudrate, ID, Model, &vi))!=0)
				{
					if (ConfirmPopup ("Initial Error", "Initial CHROMA 62000P DC power supply error. Do you want to quit?"))
					{
						QuitUserInterface (0); 
						break;
					}	
					break;
				}
			}
			
			// Set up communication parameters
			initSessionParams();
		}
		break;
	}

Error: 			
	if (error!=0)
		abortApplication (error); 
	return error;
}

int  CVICALLBACK QUIT(int panel, int event, void *callbackData, int eventData1, int eventData2)
{
	switch (event) {
	case EVENT_CLOSE:
		QuitUserInterface (0);
		closeConnection();
		break;
	}
	return 0;
}

int CVICALLBACK StopMeasure(int panel, int control, int event,
                            void *callbackData, int eventData1, int eventData2)
{
	switch (event) {
	case EVENT_COMMIT:
		closeConnection();
		
		// Play a sound for fun...
		if (CVILowLevelSupportDriverLoaded() == 1) {
			StartPCSound(440);
			Delay(0.5);
			StopPCSound();
		} else {
			Beep();
		}
		break;
	}
	return 0;
}

int CVICALLBACK radioBtnToggle (int panel, int control, int event,
								void *callbackData, int eventData1, int eventData2)
{
	int value;
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlAttribute(panel, control, ATTR_CTRL_VAL, &value);
			
			SetCtrlAttribute(panel, PANEL_RADIO_STATIC_CHARGE, ATTR_CTRL_VAL, 0);
			SetCtrlAttribute(panel, PANEL_RADIO_DYNA_STEP, ATTR_CTRL_VAL, 0);
			SetCtrlAttribute(panel, PANEL_RADIO_DYNA_RATE, ATTR_CTRL_VAL, 0);
			
			SetCtrlAttribute(panel, control, ATTR_CTRL_VAL, value);
			
			mRadioID = (value != 0) ? control : 0;
			break;
	}
	return 0;
}

/********************************************************/
/* Private function                                     */
/********************************************************/
int CVICALLBACK initSessionParams(void)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	current=0,voltage=0,vsr=0,isr=0;
	int			output_state = 0;
	char 		ID[512]="", model[512]="",msg[512]="";
	
	// First, disable all controllable components
	toggleComponents(CTRL_ENABLED);
	
	// Optional: Get instrument ID and model
	checkErr( chr62000_GetAttributeViString (vi, "", CHR62000_ATTR_ID_QUERY_RESPONSE, 512, ID));
	checkErr( chr62000_GetAttributeViString (vi, "", CHR62000_ATTR_INSTRUMENT_MODEL, 512, model));
	sprintf (msg, "*IDN? : ID [%s], Model [%s]\n", ID, model);
	
	// Set up remote conf before any communication
	checkErr( chr62000_SetRmtMode(vi, VI_TRUE) );
	
	// Prepare for voltage/current limit
	if ( strncmp(model,"62006P-100-25",13) == 0 )							  			
		current=25,voltage=100;
	else if ( strncmp(model,"62012P-30-160",13) == 0 )
		current=160,voltage=30;
	else if ( strncmp(model,"62012P-80-60",12) == 0 )
		current=60,voltage=80; 	
	else if ( strncmp(model,"62012P-100-50",13) == 0 )
		current=50,voltage=100; 	
	else if ( strncmp(model,"62012P-600-8",12) == 0 )
		current=8,voltage=600; 
		
	// Set up voltage/current limit
	checkErr( chr62000_SetVoltageLimit (vi, VI_TRUE, voltage)); 
	checkErr( chr62000_SetVoltageLimit (vi, VI_FALSE, 0.00)); 
	checkErr( chr62000_SetCurrentLimit (vi, VI_TRUE, current)); 
	checkErr( chr62000_SetCurrentLimit (vi, VI_FALSE, 0.00));
	
	// Set up the other parameters
	checkErr( chr62000_SetAPGMode (vi, 0, VI_FALSE));
	
	// Display debug message
	sprintf (msg, "%sConnected. Start measuring...", msg);
	SetCtrlVal (panelHandle, PANEL_TEXTMSG_DEBUG, msg);
	
	// Decide what kind of mode
	switch (mRadioID) {
		case PANEL_RADIO_STATIC_CHARGE: {
			GetCtrlVal (panelHandle, PANEL_NUM_STATIC_VOL, &voltage);
			GetCtrlVal (panelHandle, PANEL_NUM_STATIC_CURR, &current);
			
			// Set initial voltage/current to specified value
			checkErr( chr62000_SetVoltage (vi, voltage));
			checkErr( chr62000_SetCurrent (vi, current));
			checkErr( chr62000_SetVoltageSlew (vi, 1.0)); // unit: V/ms
			checkErr( chr62000_SetCurrentSlew (vi, 1.0)); // unit: A/ms
			
			// Set output to "ON"
			checkErr( chr62000_SetOutput (vi, VI_TRUE));
			
			// Start thread to measure Voltage and Current
			startMeasureThread();
			break;
		}
		case PANEL_RADIO_DYNA_STEP:
		case PANEL_RADIO_DYNA_RATE: {
			ViReal64	initVol, targetVol, volSlewRate,
						initCurr, targetCurr, currSlewRate;
			
			// Read settings
			GetCtrlVal(panelHandle, PANEL_NUM_DYNA_INIT_VOL, &initVol);
			GetCtrlVal(panelHandle, PANEL_NUM_DYNA_TARGET_VOL, &targetVol);
			GetCtrlVal(panelHandle, PANEL_NUM_DYNA_VOL_RATE, &volSlewRate);
			GetCtrlVal(panelHandle, PANEL_NUM_DYNA_INIT_CURR, &initCurr);
			GetCtrlVal(panelHandle, PANEL_NUM_DYNA_TARGET_CURR, &targetCurr);
			GetCtrlVal(panelHandle, PANEL_NUM_DYNA_CURR_RATE, &currSlewRate);
			
			if (mRadioID == PANEL_RADIO_DYNA_RATE) {
				// Set initial voltage/current
				checkErr( chr62000_SetVoltage (vi, initVol));
				checkErr( chr62000_SetCurrent (vi, initCurr));
				
				// Set slew rate (unit: V/S to V/mS)
				if ((targetVol - initVol) > 0) {
					checkErr( chr62000_SetVoltageSlew(vi, volSlewRate / 1000) );
				}
				if ((targetCurr - initCurr) > 0) {
					checkErr( chr62000_SetCurrentSlew(vi, currSlewRate / 1000) );
				}
	
				// Set output to "ON"
				checkErr( chr62000_SetOutput (vi, VI_TRUE));
				
				// Start thread to measure Voltage and Current
				startMeasureThread();
				
				// Set target voltage/current
				checkErr( chr62000_SetVoltage (vi, targetVol));
				checkErr( chr62000_SetCurrent (vi, targetCurr));
			} else {
				ViReal64 funcData[6] = { initVol, targetVol, volSlewRate,
										 initCurr, targetCurr, currSlewRate };
				
				// Start thread to controll step currents
				quitStepThread = 0;
				CmtScheduleThreadPoolFunction(DEFAULT_THREAD_POOL_HANDLE, stepDynamicCurrThread, funcData, &stepFunctionID);
				
				// Start thread to measure Voltage and Current
				startMeasureThread();
			}
			
			break;
		}
	}
	
Error: 			
	if (error != 0) {
		abortApplication (error);
	}
	return error;
}

void startMeasureThread() {
	// Start thread to measure Voltage and Current
	quit = 0;
	CmtScheduleThreadPoolFunction(DEFAULT_THREAD_POOL_HANDLE, fetchVolCurrThread, NULL, &fetchFunctionID);
}

int closeConnection()
{
	ViStatus	error = VI_SUCCESS;
	
	// First, enable all controllable components
	toggleComponents(CTRL_ENABLED);
	
	// Display stopping msg and disable stop button
	SetCtrlVal (panelHandle, PANEL_TEXTMSG_DEBUG, "Stop measuring. Please wait ......");
	SetCtrlAttribute(panelHandle, PANEL_BTN_STOP, ATTR_DIMMED, CTRL_DISABLED);
	
	// Then, close EL connection
	if (vi != NULL) {
		// First, stop looping dynamic discharge currents
		if (stepFunctionID != NULL) {
			quitStepThread = 1;
			CmtWaitForThreadPoolFunctionCompletion(DEFAULT_THREAD_POOL_HANDLE, stepFunctionID, OPT_TP_PROCESS_EVENTS_WHILE_WAITING);
			CmtReleaseThreadPoolFunctionID(DEFAULT_THREAD_POOL_HANDLE, stepFunctionID);
			stepFunctionID = 0;
		}
		
		// Then, stop measuring
		if (fetchFunctionID != NULL) {
			quit = 1;
			CmtWaitForThreadPoolFunctionCompletion(DEFAULT_THREAD_POOL_HANDLE, fetchFunctionID, OPT_TP_PROCESS_EVENTS_WHILE_WAITING);
			CmtReleaseThreadPoolFunctionID(DEFAULT_THREAD_POOL_HANDLE, fetchFunctionID);
			fetchFunctionID = 0;
		}
		
		// Set output "OFF"
		checkErr( chr62000_SetOutput(vi, VI_FALSE) );

		// Set remote "OFF"
		checkErr( chr62000_SetRmtMode(vi, VI_FALSE) );
	
		// Close session
		checkErr( chr62000_close(vi) );
		Delay (0.5);
		vi = 0;
	}
	
	// Clear message and enable stop button
	SetCtrlVal(panelHandle, PANEL_TEXTMSG_DEBUG, "");
	SetCtrlAttribute(panelHandle, PANEL_BTN_STOP, ATTR_DIMMED, CTRL_ENABLED);
	
Error: 			
	if (error != 0) {
		abortApplication (error);
	}
	return error;
}

void toggleComponents(int enable)
{
	SetCtrlAttribute(panelHandle, PANEL_RING_COMPORT, ATTR_DIMMED, enable);
	SetCtrlAttribute(panelHandle, PANEL_RING_BAUDRATE, ATTR_DIMMED, enable);
	SetCtrlAttribute(panelHandle, PANEL_CB_SIMULATE, ATTR_DIMMED, enable);
}

int CVICALLBACK fetchVolCurrThread(void *functionData)
{
	ViStatus	error = VI_SUCCESS;
	
	// Measure Voltage and Current
	char		str[60];
	ViReal64	current=0,voltage=0;
	
	// Disable start button
	SetCtrlAttribute(panelHandle, PANEL_BTN_NEXT, ATTR_DIMMED, CTRL_DISABLED);
	
	// Set up text color to red (Highlighted)
	SetCtrlAttribute(panelHandle, PANEL_MEASURE_CURR, ATTR_TEXT_COLOR, VAL_RED);
	SetCtrlAttribute(panelHandle, PANEL_MEASURE_VOL, ATTR_TEXT_COLOR, VAL_RED);
	
	while (!quit) {
		Delay (0.5);
		
		checkErr( chr62000_Measure(vi, CHR62000_VAL_MEAS_VOLT, &voltage) );
		sprintf(str, "%.4f", voltage);
		SetCtrlVal(panelHandle, PANEL_MEASURE_VOL, str);
		
		checkErr( chr62000_Measure(vi, CHR62000_VAL_MEAS_CURR, &current) );
		sprintf(str, "%.4f", current);
		SetCtrlVal(panelHandle, PANEL_MEASURE_CURR, str);
	}
	
	// Enable start button
	SetCtrlAttribute(panelHandle, PANEL_BTN_NEXT, ATTR_DIMMED, CTRL_ENABLED);
	
	// Set up text color to black (non-highlighted)
	sprintf(str, "%.4f", 0.0);
	SetCtrlVal(panelHandle, PANEL_MEASURE_CURR, str);
	SetCtrlVal(panelHandle, PANEL_MEASURE_VOL, str);
	SetCtrlAttribute(panelHandle, PANEL_MEASURE_CURR, ATTR_TEXT_COLOR, VAL_BLACK);
	SetCtrlAttribute(panelHandle, PANEL_MEASURE_VOL, ATTR_TEXT_COLOR, VAL_BLACK);
	
Error: 			
	if (error != 0) {
		abortApplication (error);
	}
	return error;
}

int CVICALLBACK stepDynamicCurrThread(void *functionData)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	*data = (ViReal64 *) functionData;
	ViReal64	initVol		= data[0],
				targetVol	= data[1],
				volSlewRate	= data[2],
				initCurr	= data[3],
				targetCurr	= data[4],
				currSlewRate = data[5];
	ViReal64	vol_now = initVol,
				curr_now = initCurr;

	// Set initial voltage/current
	checkErr( chr62000_SetVoltage (vi, initVol) );
	checkErr( chr62000_SetCurrent (vi, initCurr) );
	
	// Set slew rate
	checkErr( chr62000_SetVoltageSlew(vi, 1.0) );
	checkErr( chr62000_SetCurrentSlew(vi, 1.0) );
	
	// Set output to "ON"
	checkErr( chr62000_SetOutput (vi, VI_TRUE));
	
	while ((vol_now < targetVol || curr_now < targetCurr) && !quitStepThread) {
		Delay(1.0);
		
		// Set voltage/current by step
		vol_now += volSlewRate;
		vol_now = (vol_now > targetVol) ? targetVol : vol_now;
		checkErr( chr62000_SetVoltage(vi, vol_now) );
		curr_now += currSlewRate;
		curr_now = (curr_now > targetCurr) ? targetCurr : curr_now;
		checkErr( chr62000_SetCurrent(vi, curr_now) );
		
		//printf("Now voltage: %f, current: %f\n", vol_now, curr_now);
	}
	
Error: 			
	if (error != 0) {
		abortApplication (error);
	}
	return error;
}

int CVICALLBACK abortApplication(ViStatus error)
{
	char		message[200]="";
	
	// First, enable all controllable components
	toggleComponents(CTRL_ENABLED);
	
	if (error>=0xbffa4001 && error <=0xbffa4003) // out of range
	{
		if (error==0xbffa4001)
			sprintf (message, "The value you passed is out of range.");
		else if (error==0xbffa4002)			
			sprintf (message, "The value you passed is out of range of voltage limit.");
		else if (error==0xbffa4003)
			sprintf (message, "The value you passed is out of range of current limit.");
		MessagePopup ("Error", message); 
	}
	else if (error >= 3220963328)  //0xBFFC00000
	{
		MessagePopup ("Error", "I/O or System error occured. Program now abort.");  		
		QuitUserInterface (0);
		chr62000_close (vi);
		Delay (0.5); 
	} else {
		if (ConfirmPopup ("Initial Error", "Initial CHROMA 6310A DC electronic load error. Do you want to quit?"))
		{
			QuitUserInterface (0);
			if (vi != NULL) {
				chr62000_SetRmtMode (vi, VI_FALSE);
				Delay (0.5); 
				chr62000_close (vi);
				Delay (0.5); 
			}
		}
	}
	
	return 0;
}
