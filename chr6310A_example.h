/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2014. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1       /* callback function: QUIT */
#define  PANEL_BTN_NEXT                   2       /* control type: command, callback function: next */
#define  PANEL_CB_SIMULATE                3       /* control type: radioButton, callback function: (none) */
#define  PANEL_RING_COMPORT               4       /* control type: ring, callback function: (none) */
#define  PANEL_RING_BAUDRATE              5       /* control type: ring, callback function: (none) */
#define  PANEL_BTN_STOP                   6       /* control type: command, callback function: StopMeasure */
#define  PANEL_MEASURE_CURR               7       /* control type: string, callback function: (none) */
#define  PANEL_MEASURE_VOL                8       /* control type: string, callback function: (none) */
#define  PANEL_RADIO_STATIC_CHARGE        9       /* control type: radioButton, callback function: radioBtnToggle */
#define  PANEL_RADIO_DYNA_STEP            10      /* control type: radioButton, callback function: radioBtnToggle */
#define  PANEL_RADIO_DYNA_RATE            11      /* control type: radioButton, callback function: radioBtnToggle */
#define  PANEL_NUM_STATIC_CURR            12      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_DYNA_INIT_VOL          13      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_DYNA_TARGET_VOL        14      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_DYNA_VOL_RATE          15      /* control type: numeric, callback function: (none) */
#define  PANEL_TEXTMSG_5                  16      /* control type: textMsg, callback function: (none) */
#define  PANEL_NUM_DYNA_INIT_CURR         17      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_DYNA_TARGET_CURR       18      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_DYNA_CURR_RATE         19      /* control type: numeric, callback function: (none) */
#define  PANEL_TEXTMSG_4                  20      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_DEBUG              21      /* control type: textMsg, callback function: (none) */
#define  PANEL_NUM_STATIC_VOL             22      /* control type: numeric, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK next(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QUIT(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK radioBtnToggle(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopMeasure(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
